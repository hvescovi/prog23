class Pessoa:

    def __init__(self, nome="", email="", telefone=""):
        self.__nome = nome
        # se foi informado email e ele não é válido
        if (email != "") and ("@" not in email):
            raise Exception("O valor fornecido não é um email")
        self.__email = email
        self.__telefone = telefone
     
    @property 
    def nome(self):
        return self.__nome
    
    @nome.setter
    def nome(self, novo_nome):
        self.__nome = novo_nome

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, novo_email):
        if "@" not in novo_email:
            raise Exception("O valor fornecido não é um email")
        self.__email = novo_email

    @property
    def telefone(self):
        return self.__telefone

    @telefone.setter
    def telefone(self, novo_telefone):
        self.__telefone = novo_telefone

    def __str__(self):
        return f'''Nome: {self.nome}, 
Email: {self.email},
Telefone: {self.telefone}'''

# teste da classe
p1 = Pessoa()
p1.nome = "João da Silva"
p1.email = "josilva@gmail.com"
p1.telefone = "47 9 9123 4567"

print(p1)