# https://stackoverflow.com/questions/63911484/how-do-i-display-text-on-the-screen-in-pygame

import time
import pygame
from pygame.locals import *

# Constants
blue = (0,0,255)
WINDOW_WIDTH  = 500
WINDOW_HEIGHT = 500

# função definida para exibir mensagem na tela
def show_text( msg, color, x=WINDOW_WIDTH//2, y=WINDOW_WIDTH//2 ):
    global WINDOW
    text = font.render( msg, True, color)
    WINDOW.blit(text, ( x, y ) )

pygame.init()
WINDOW = pygame.display.set_mode((WINDOW_WIDTH,WINDOW_HEIGHT))
pygame.display.set_caption("Text")

# criar a fonte (é feito só uma vez)
font = pygame.font.SysFont(None, 25)

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()

    WINDOW.fill( ( 255, 255, 255 ) )   # fill screen with white background

    show_text("This is a message!", blue)

    pygame.display.update()
