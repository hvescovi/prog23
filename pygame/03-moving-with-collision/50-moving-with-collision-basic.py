# https://www.pygame.org/docs/tut/newbieguide.html

import pygame

pygame.init()

screen = pygame.display.set_mode((800,600))

clock = pygame.time.Clock()

# posições iniciais da bola
x = 320
y = 320

# área de um retângulo
rect = pygame.Rect(100, 100, 30, 120) # x, y, base, altura
# rect = pygame.Rect(*screen.get_rect().center, 0, 0).inflate(100, 100)

while True:
    # Process player inputs.
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit     

    # Do logical updates here.
    # ...

    # obtém x e y anteriores
    x_ant = x
    y_ant = y

    # captura de eventos do teclado
    pk = pygame.key.get_pressed()
    if pk[pygame.K_LEFT]:
        x -= 1
    if pk[pygame.K_RIGHT]:
        x += 1
    if pk[pygame.K_UP]:
        y -= 1
    if pk[pygame.K_DOWN]:
        y += 1

    # obtém a posição do círculo em forma de ponto
    point = (x, y)

    # verifica se houve colisão
    collide = rect.collidepoint(point)

    # se houve colisão
    if collide:
        # restaura posições anteriores
        x = x_ant
        y = y_ant

    screen.fill("gray")  # Fill the display with a solid color

    pygame.draw.circle(screen, (10, 10, 10), [x, y], 15, 5)
    pygame.draw.rect(screen, (140, 0, 90), rect) # desenho do retângulo


    # Render the graphics here.
    # ...

    pygame.display.flip()  # Refresh on-screen display
    clock.tick(60)         # wait until next frame (at 60 FPS)