import pygame

# inicializa o pygame
pygame.init()

# define o tamanho da tela
screen = pygame.display.set_mode((1280,720))

# inicia um relógio lógico do jogo
clock = pygame.time.Clock()

# especifica um jogador (x, y, base, altura)
jogador_rect = pygame.Rect(10, 10, 20, 20)

# especifica dois obstáculos
ob1_rect = pygame.Rect(200, 200, 100, 100)
ob2_rect = pygame.Rect(300, 400, 150, 150)

# especifica cores
AZUL = (0, 0, 255)
COR1 = (123, 45, 67)

# inicia repetição para permitir a execução do jogo
while True:

    # código padrão para encerramento do jogo
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit     

    # guarda x e y anteriores do jogador
    x_ant = jogador_rect.x
    y_ant = jogador_rect.y

    # captura de eventos do teclado
    pk = pygame.key.get_pressed()

    # verifica qual tela foi pressionada e toma a ação apropriada
    if pk[pygame.K_LEFT]:
        jogador_rect.x -= 1
    if pk[pygame.K_RIGHT]:
        jogador_rect.x += 1
    if pk[pygame.K_UP]:
        jogador_rect.y -= 1
    if pk[pygame.K_DOWN]:
        jogador_rect.y += 1
    if pk[pygame.K_q]:
        break

    # verifica se houve colisão do jogador com os obstáculos
    colidiu1 = jogador_rect.colliderect(ob1_rect)
    colidiu2 = jogador_rect.colliderect(ob2_rect)

    # se houve colisão
    if colidiu1 or colidiu2:
        # restaura posições anteriores
        jogador_rect.x = x_ant
        jogador_rect.y = y_ant

    # desenha um fundo verde
    screen.fill("green")

    # desenha os obstáculos
    pygame.draw.rect(screen, AZUL, ob1_rect)
    pygame.draw.rect(screen, AZUL, ob2_rect)

    # desenha o jogador
    pygame.draw.rect(screen, COR1, jogador_rect)

    # atualiza a tela
    pygame.display.flip()  

    # tempo de espera - 60 frames por segundo
    clock.tick(60)