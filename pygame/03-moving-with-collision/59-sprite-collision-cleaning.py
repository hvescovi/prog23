# https://www.makeuseof.com/pygame-collision-detection-physics/

import pygame

pygame.init()
screen = pygame.display.set_mode((800,600))
clock = pygame.time.Clock()

# classe JOGADOR!
class Player(pygame.sprite.Sprite):

    # construtor
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((32, 32))
        self.rect = self.image.get_rect(topleft=(x, y))
        self.y_velocity = 1

    # verificação de teclas
    def check_keys(self):
        # captura alguma eventual tecla que foi pressionada
        pk = pygame.key.get_pressed()
        # alguma tecla especial foi pressionada?
        if pk[pygame.K_LEFT]:
            self.rect.x -= 1
        if pk[pygame.K_RIGHT]:
            self.rect.x += 1
        if pk[pygame.K_UP]:
            self.rect.y -= 2
        if pk[pygame.K_DOWN]:
            self.rect.y += 2    

    def salvar_xy(self):
        self.antes_x = self.rect.x
        self.antes_y = self.rect.y

    def restaurar_xy(self):    
        self.rect.x = self.antes_x
        self.rect.y = self.antes_y

    # verificar se houve alguma atualização
    # na situação do jogador
    def update(self, pg):
        self.salvar_xy()
        self.rect.y += self.y_velocity        
        self.check_keys()
        colidiu = pygame.sprite.spritecollide(self, pg, False)
        if colidiu:
            self.restaurar_xy()
    
# classe OBSTÁCULO!
class Platform(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.image = pygame.Surface((width, height))
        self.rect = self.image.get_rect(topleft=(x, y))

# posições iniciais do player
x = 50
y = 320

# cria jogador
player = Player(x, y)
# cria grupo de jogadores
player_group = pygame.sprite.Group()
# adiciona jogador no grupo :-)
player_group.add(player)

# cria segundo jogador
player2 = Player(x+40, y-210)
# adiciona o segundo jogador no grupo
player_group.add(player2)

# cria obstáculos
platform = Platform(50, 400, 100, 20)
platform2 = Platform(190, 400, 100, 20)
platform3 = Platform(50, 200, 100, 20)
# cria grupo de obstáculos
platform_group = pygame.sprite.Group()
# adiciona obstáculos no grupo
platform_group.add(platform)
platform_group.add(platform2)
platform_group.add(platform3)

# Initialize pygame and create window
pygame.init()
screen = pygame.display.set_mode((640, 480))

# Main game loop
running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        
    # pede para cada jogador se "atualizar"
    player_group.update(platform_group)
    
    screen.fill("yellow")
    player_group.draw(screen)
    platform_group.draw(screen)
    pygame.display.flip()

    clock.tick(60)

pygame.quit()