# https://www.makeuseof.com/pygame-collision-detection-physics/

import pygame

pygame.init()

screen = pygame.display.set_mode((800,600))

clock = pygame.time.Clock()

class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((32, 32))
        self.rect = self.image.get_rect(topleft=(x, y))
        
class Platform(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.image = pygame.Surface((width, height))
        self.rect = self.image.get_rect(topleft=(x, y))

# posições iniciais do player
x = 50
y = 320

player = Player(x, y)
player_group = pygame.sprite.Group()
player_group.add(player)

# apenas adicionei mais obstáculos
# a colisão de grupo (linha 71) já vai verificar tudo :-)
platform = Platform(50, 400, 100, 20)
platform2 = Platform(190, 400, 100, 20)
platform3 = Platform(50, 200, 100, 20)
platform_group = pygame.sprite.Group()
platform_group.add(platform)
platform_group.add(platform2)
platform_group.add(platform3)

# Initialize pygame and create window
pygame.init()
screen = pygame.display.set_mode((640, 480))

# Main game loop
running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    # salva posições antes de modificá-las
    antes_x = player.rect.x
    antes_y = player.rect.y

    # captura de eventos do teclado
    pk = pygame.key.get_pressed()

    # verifica se tem que fazer algo
    if pk[pygame.K_LEFT]:
        player.rect.x -= 1
    if pk[pygame.K_RIGHT]:
        player.rect.x += 1
    if pk[pygame.K_UP]:
        player.rect.y -= 1
    if pk[pygame.K_DOWN]:
        player.rect.y += 1    

    # colidiu?
    collided = pygame.sprite.spritecollide(player, platform_group, False)

    # se colidiu...
    if collided:
        # restaura posições
        player.rect.x = antes_x
        player.rect.y = antes_y

    screen.fill("yellow")
    player_group.draw(screen)
    platform_group.draw(screen)
    pygame.display.flip()

    clock.tick(60)

pygame.quit()