# https://stackoverflow.com/questions/16183265/how-to-move-sprite-in-pygame

import pygame
import os

# let's address the class a little later..

pygame.init()
screen = pygame.display.set_mode((640, 400))
# you only need to call the following once,so pull them out of the  while loop.
caminho = os.path.dirname(os.path.abspath(__file__))
# concatenar o caminho com o nome do arquivo da imagem
arquivo_imagem = os.path.join(caminho, 'player.png')

jogador = pygame.image.load(arquivo_imagem)
clock = pygame.time.Clock()

running = True
while running:
    event = pygame.event.poll()
    if event.type == pygame.QUIT:
        running = False

    screen.fill("lightgray")

    # coloca a imagem na tela
    screen.blit(jogador, (0, 0))

    pygame.display.update() # Just do one thing, update/flip.

    clock.tick(40) # This call will regulate your FPS (to be 40 or less)
