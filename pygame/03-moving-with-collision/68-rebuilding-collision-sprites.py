import pygame

# inicializa o pygame
pygame.init()

# define o tamanho da tela
screen = pygame.display.set_mode((1280,720))

# inicia um relógio lógico do jogo
clock = pygame.time.Clock()

# especifica um jogador (x, y, base, altura)
jogador_sprite = pygame.sprite.Sprite()
jogador_sprite.image = pygame.Surface((32, 32))
jogador_sprite.rect = pygame.Rect(10, 10, 20, 20)

# especifica dois obstáculos
ob1_sprite = pygame.sprite.Sprite()
ob1_sprite.image = pygame.Surface((100, 100))
ob1_sprite.rect = ob1_sprite.image.get_rect(topleft=(200, 200))

ob2_sprite = pygame.sprite.Sprite()
ob2_sprite.image = pygame.Surface((150, 150))
ob2_sprite.rect = ob2_sprite.image.get_rect(topleft=(300, 400))

# cria um grupo de sprites
obstaculos = pygame.sprite.Group()
# inserir os obstáculos no grupo
obstaculos.add(ob1_sprite, ob2_sprite)

# especifica cores
AZUL = (0, 0, 255)
COR1 = (123, 45, 67)

# inicia repetição para permitir a execução do jogo
while True:

    # código padrão para encerramento do jogo
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit     

    # guarda x e y anteriores do jogador
    x_ant = jogador_sprite.rect.x
    y_ant = jogador_sprite.rect.y

    # captura de eventos do teclado
    pk = pygame.key.get_pressed()

    # verifica qual tela foi pressionada e toma a ação apropriada
    if pk[pygame.K_LEFT]:
        jogador_sprite.rect.x -= 1
    if pk[pygame.K_RIGHT]:
        jogador_sprite.rect.x += 1
    if pk[pygame.K_UP]:
        jogador_sprite.rect.y -= 1
    if pk[pygame.K_DOWN]:
        jogador_sprite.rect.y += 1
    if pk[pygame.K_q]:
        break

    # verifica se houve colisão do jogador com os obstáculos
    quem_colidiu = pygame.sprite.spritecollide(jogador_sprite, obstaculos, False)

    # se houve colisão
    if len(quem_colidiu) > 0:
        jogador_sprite.rect.x = x_ant
        jogador_sprite.rect.y = y_ant

    # desenha um fundo verde
    screen.fill("green")

    # desenha os obstáculos
    obstaculos.draw(screen)
    
    # desenha o jogador
    pygame.draw.rect(screen, COR1, jogador_sprite.rect)

    # atualiza a tela
    pygame.display.flip()  

    # tempo de espera - 60 frames por segundo
    clock.tick(60)