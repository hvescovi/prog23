import pygame
import copy

pygame.init()

# tamanho da janela
X_SIZE = 650
Y_SIZE = 500
window = pygame.display.set_mode((X_SIZE, Y_SIZE))

# pega a referência temporal para poder fazer espera mais à frente
clock = pygame.time.Clock()

# classes :-)
class Figura:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.sentido_x = 1
        self.sentido_y = 1
        self.velocidade_x = 2 # 2 para muita colisão y
        self.velocidade_y = 1 # 5 para muita colisão y
    def inverte_sentido_x(self):
        self.sentido_x *= -1
        #print("virou x em", self.y, "agora é ", self.sentido_x)
    def inverte_sentido_y(self):
        self.sentido_y *= -1
        #print("virou y em ", self.x, "agora é ", self.sentido_y)
    def andar(self):
        self.x += self.velocidade_x * self.sentido_x
        self.y += self.velocidade_y * self.sentido_y

class Retangulo(Figura):
    def __init__(self, x, y, x2, y2):
        super().__init__(x, y)
        self.x2 = x2
        self.y2 = y2
        self.color = (255, 0, 0) # cor padrão
    def desenhar(self):
        pygame.draw.rect(window, self.color, (self.x, self.y, self.x2, self.y2)) 
    def colidiu(self, outro_objeto):    
        area_varredura = pygame.Rect(self.x, self.y, self.x2, self.y2)
        return area_varredura.collidepoint((outro_objeto.x, outro_objeto.y))

class Circulo(Figura):
    def __init__(self, x, y, raio, borda):
        super().__init__(x, y)
        self.raio = raio
        self.borda = borda
    def desenhar(self):
        pygame.draw.circle(window, (10, 10, 10), [self.x, self.y], self.raio, self.borda)
    def colidiu(self, outro_objeto):    
        area_varredura = pygame.Rect(self.x - self.raio, self.y - self.raio, self.x + self.raio, self.y + self.raio)
        return area_varredura.collidepoint((outro_objeto.x, outro_objeto.y))
           
# definição do quadradão
grandao = Retangulo(20, 40, 80, 380)
grandao2 = Retangulo(150, 150, 120, 220)
grandao3 = Retangulo(300, 220, 50, 180)
grandao4 = Retangulo(450, 350, 120, 120)
grandao5 = Retangulo(350, 50, 190, 150)

vermelho = (255, 0, 0)
verde = (0, 255, 0)
azul = (0, 0, 255)

grandao.color = vermelho
grandao2.color = verde
grandao3.color = azul
grandao4.color = verde
grandao5.color = vermelho

# definição da bolinha
bolinha = Circulo(5, 164, 15, 5) # y = 300
bolinha2 = Circulo(115, 500, 25, 7)
bolinha3 = Circulo(125, 400, 18, 4)
bolinha4 = Circulo(10, 100, 14, 2)

def verifica_colisao(quadrado, bola):
    if quadrado.colidiu(bola):

        # vai tentar descobrir qual lado não colidiu
        salto_x = abs(bola.velocidade_x)
        salto_y = abs(bola.velocidade_y)

        while True:
            # pega 4 pontos nas extremidades com deslocamento
            px_antes = copy.copy(bola)
            px_antes.x -= salto_x
            px_depois = copy.copy(bola)
            px_depois.x += salto_x
            py_antes = copy.copy(bola)
            py_antes.y -= salto_y
            py_depois = copy.copy(bola)
            py_depois.y += salto_y
            # cria objetos fictícios nestes 4 locais  
            colidiu_x_antes = quadrado.colidiu(px_antes)
            colidiu_x_depois = quadrado.colidiu(px_depois)
            colidiu_y_antes = quadrado.colidiu(py_antes)
            colidiu_y_depois = quadrado.colidiu(py_depois)

            # quicando na parede vertical: colidiu y antes e depois, e: 
            # (não colidiu x antes) ou (não colidiu x depois)
            if (colidiu_y_antes and colidiu_y_depois) and ( (not colidiu_x_antes) or (not colidiu_x_depois) ):
                # não pode quicar "por dentro"            
                bola.inverte_sentido_x() # quicando na vertical: inverte x
                break
            else:
                # quicando na horizontal: x colidiram, um dos y não colidiu
                if (colidiu_x_antes and colidiu_x_depois) and ( (not colidiu_y_antes) or (not colidiu_y_depois) ):
                    bola.inverte_sentido_y()  
                    break
                else:
                    # situação especial: bateu na quina!
                    # mas são 4 quinas :-/
                    q1 = colidiu_x_depois and colidiu_y_depois and (not colidiu_x_antes) and (not colidiu_y_antes)
                    q2 = colidiu_x_antes and colidiu_y_depois and (not colidiu_x_depois) and (not colidiu_y_antes)
                    q3 = colidiu_x_antes and colidiu_y_antes and (not colidiu_x_depois) and (not colidiu_y_depois)
                    q4 = colidiu_x_depois and colidiu_y_antes and (not colidiu_x_antes) and (not colidiu_y_depois)

                    # bateu em alguma quina?
                    if q1 or q2 or q3 or q4:
                        
                        bola.inverte_sentido_x()  
                        bola.inverte_sentido_y()  
                            
                        print("quina ", end="")
                        break
                        # OBS: pra ficar perfeito, precisava ver se onde veio a bolinha
                        # para poder fazer a quicagem correta na quinta
                        # se vem da esquerda e bateu na quinta da direita precisava refletir para cima apenas
                    else:
                        # vamos ampliar o alcance de expansão dos pontos fictícios
                        # para tentar novamente
                        salto_x = salto_x + 1
                        salto_y = salto_y + 1
                        print("Fiquei na dúvida, ampliando...")

run = True
while run:

    # código obrigatório para finalizar bem o programa
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        
    # desenha informações na tela
    window.fill("purple")    
    grandao.desenhar()
    grandao2.desenhar()
    grandao3.desenhar()
    grandao4.desenhar()
    grandao5.desenhar()
    bolinha.desenhar()
    bolinha2.desenhar()
    bolinha3.desenhar()
    bolinha4.desenhar()


    # desenha os objetos efetivamente
    pygame.display.flip()

    # o circulo vazou a borda da janela?
    if bolinha.x > X_SIZE or bolinha.x < 0:
       bolinha.inverte_sentido_x()
    if bolinha.y > Y_SIZE or bolinha.y < 0:    
       bolinha.inverte_sentido_y()
    
    # o circulo 2 vazou a borda da janela?
    if bolinha2.x > X_SIZE or bolinha2.x < 0:
       bolinha2.inverte_sentido_x()
    if bolinha2.y > Y_SIZE or bolinha2.y < 0:    
       bolinha2.inverte_sentido_y()

    # o circulo 3 vazou a borda da janela?
    if bolinha3.x > X_SIZE or bolinha3.x < 0:
       bolinha3.inverte_sentido_x()
    if bolinha3.y > Y_SIZE or bolinha3.y < 0:    
       bolinha3.inverte_sentido_y()

    # o circulo 4 vazou a borda da janela?
    if bolinha4.x > X_SIZE or bolinha4.x < 0:
       bolinha4.inverte_sentido_x()
    if bolinha4.y > Y_SIZE or bolinha4.y < 0:    
       bolinha4.inverte_sentido_y()
    
    # houve colisão?
    verifica_colisao(grandao, bolinha)
    verifica_colisao(grandao2, bolinha)
    verifica_colisao(grandao3, bolinha)
    verifica_colisao(grandao4, bolinha)
    verifica_colisao(grandao5, bolinha)

    # houve colisão com a outra bola?
    verifica_colisao(grandao, bolinha2)
    verifica_colisao(grandao2, bolinha2)
    verifica_colisao(grandao3, bolinha2)
    verifica_colisao(grandao4, bolinha2)
    verifica_colisao(grandao5, bolinha2)

    # houve colisão com a outra bola?
    verifica_colisao(grandao, bolinha3)
    verifica_colisao(grandao2, bolinha3)
    verifica_colisao(grandao3, bolinha3)
    verifica_colisao(grandao4, bolinha3)
    verifica_colisao(grandao5, bolinha3)

    # houve colisão com a outra bola?
    verifica_colisao(grandao, bolinha4)
    verifica_colisao(grandao2, bolinha4)
    verifica_colisao(grandao3, bolinha4)
    verifica_colisao(grandao4, bolinha4)
    verifica_colisao(grandao5, bolinha4)
    
    # copia a bolinha anterior, antes de atualizar a nova
    bant = copy.copy(bolinha)
    bant2 = copy.copy(bolinha2)
    bant3 = copy.copy(bolinha3)
    bant4 = copy.copy(bolinha4)

    # varia a posição doS círculoSSSSS
    bolinha.andar()
    bolinha2.andar()
    bolinha3.andar()
    bolinha4.andar()
    
    # espera um pouco
    clock.tick(60)     

pygame.quit()
exit()
