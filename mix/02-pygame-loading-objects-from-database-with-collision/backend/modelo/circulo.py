from backend.geral.config import *
from backend.geral.config_jogos import *

class Circulo(db.Model):
    # atributo auto-gerado
    id = db.Column(db.Integer, primary_key=True)
    
    # o valor default é usado para persistência
    # no objeto, em momento de criação, antes da persistência o valor permanece NoneType :-/
    x = db.Column(db.Integer) 
    y = db.Column(db.Integer) 
    raio = db.Column(db.Integer, default = 7) 
    borda = db.Column(db.Integer, default = 2)

    sx = db.Column(db.Integer, default=1) # sentido x
    sy = db.Column(db.Integer, default=1) # sentido y
    vx = db.Column(db.Integer, default=2) # velocidade x
    vy = db.Column(db.Integer, default=3) # velocidade x
    
    def desenhar(self, window, color):
        pygame.draw.circle(window, color, [self.x, self.y], self.raio, self.borda)
    
    def colidiu(self, outro_objeto):
        area_varredura = pygame.Rect(self.x - self.raio, self.y - self.raio, self.x + self.raio, self.y + self.raio)
        return area_varredura.collidepoint((outro_objeto.x, outro_objeto.y))
    def inverte_sentido_x(self):
        self.sx *= -1
    def inverte_sentido_y(self):
        self.sy *= -1
    def andar(self):
        self.x += self.vx * self.sx
        self.y += self.vy * self.sy
    
    nome = db.Column(db.Text)

    def __str__(self):
        return f'vel: {self.vx}'
    
    # expressao da classe no formato json
    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "vx": self.vx,
        }