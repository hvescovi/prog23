from backend.geral.config import *
from backend.geral.config_jogos import *

class Retangulo(db.Model):
    # atributo auto-gerado
    id = db.Column(db.Integer, primary_key=True)
    
    retangulo = db.Column(db.Text) # guarda as 4 coordenadas do retangulo
    sentido = db.Column(db.Integer, default=0) # 1 ou -1
    vx = db.Column(db.Integer, default=0) # velocidade x
    vy = db.Column(db.Integer, default=0) # velocidade x
    
    def desenhar(self, window, color):
        coords = self.retangulo.split(",")
        temp = pygame.Rect(int(coords[0]), int(coords[1]), 
                           int(coords[2]), int(coords[3]))
        pygame.draw.rect(window, color, temp) # desenho do retângulo   
    
    def colidiu(self, outro_objeto):
        #return outro_objeto.retangulo.collidepoint((self.x, self.y))
        coords = self.retangulo.split(",")
        temp = pygame.Rect(int(coords[0]), int(coords[1]), 
                           int(coords[2]), int(coords[3]))
        #area_varredura = pygame.Rect(self.x, self.y, self.x2, self.y2)
        return temp.collidepoint((outro_objeto.x, outro_objeto.y))
    def inverte_sentido(self):
        self.sentido *= -1
    def andar(self):
        self.x += self.vx * self.sentido
    # um nome para a figura
    nome = db.Column(db.Text)

    def __str__(self):
        return f'sentido {self.sentido}, vel: {self.vx}'
    
    # expressao da classe no formato json
    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "vx": self.vx,
            "sentido": self.sentido
        }