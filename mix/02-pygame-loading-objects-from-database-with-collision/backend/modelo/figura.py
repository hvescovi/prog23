from backend.geral.config import *

class Figura(db.Model):
    # atributo auto-gerado
    id = db.Column(db.Integer, primary_key=True)
    # posições do jogo
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    sentido = db.Column(db.Integer) # 1 ou -1
    velocidade_x = db.Column(db.Integer) # padrão = 1
    def colidiu(self, outro_objeto):
        return outro_objeto.retangulo.collidepoint((self.x, self.y))
    def inverte_sentido(self):
        self.sentido *= -1
    def andar(self):
        self.x += self.velocidade_x * self.sentido
    # um nome para a figura
    nome = db.Column(db.Text)

    def __str__(self):
        return f'{self.nome} está em {self.x} e {self.y}, sentido {self.sentido}, vel: {self.velocidade_x}'
    
    # expressao da classe no formato json
    def json(self):
        return {
            "id": self.id,
            "x": self.x,
            "y": self.y,
            "nome": self.nome,
            "velocidade_x": self.velocidade_x,
            "sentido": self.sentido
        }