# referência:
# https://stackoverflow.com/questions/29640685/how-do-i-detect-collision-in-pygame

# carregar informações de armazenamento
from backend.geral.config import *
from backend.modelo.retangulo import *
from backend.modelo.circulo import *

from backend.geral.config_jogos import *

import copy

import random

def verifica_colisao(quadrado, bola):
    if quadrado.colidiu(bola):

        # vai tentar descobrir qual lado não colidiu
        salto_x = abs(bola.vx)
        salto_y = abs(bola.vy)

        # inicialmente, não precisa ampliar salto para detectar situação
        ampliar_salto = 0

        while True:
            # pega 4 pontos nas extremidades com deslocamento
            px_antes = copy.copy(bola)
            px_antes.x -= salto_x
            px_depois = copy.copy(bola)
            px_depois.x += salto_x
            py_antes = copy.copy(bola)
            py_antes.y -= salto_y
            py_depois = copy.copy(bola)
            py_depois.y += salto_y
            # cria objetos fictícios nestes 4 locais  
            colidiu_x_antes = quadrado.colidiu(px_antes)
            colidiu_x_depois = quadrado.colidiu(px_depois)
            colidiu_y_antes = quadrado.colidiu(py_antes)
            colidiu_y_depois = quadrado.colidiu(py_depois)

            # quicando na parede vertical: colidiu y antes e depois, e: 
            # (não colidiu x antes) ou (não colidiu x depois)
            if (colidiu_y_antes and colidiu_y_depois) and ( (not colidiu_x_antes) or (not colidiu_x_depois) ):
                # não pode quicar "por dentro"            
                bola.inverte_sentido_x() # quicando na vertical: inverte x
                break
            else:
                # quicando na horizontal: x colidiram, um dos y não colidiu
                if (colidiu_x_antes and colidiu_x_depois) and ( (not colidiu_y_antes) or (not colidiu_y_depois) ):
                    bola.inverte_sentido_y()  
                    break
                else:
                    # situação especial: bateu na quina!
                    # mas são 4 quinas :-/
                    q1 = colidiu_x_depois and colidiu_y_depois and (not colidiu_x_antes) and (not colidiu_y_antes)
                    q2 = colidiu_x_antes and colidiu_y_depois and (not colidiu_x_depois) and (not colidiu_y_antes)
                    q3 = colidiu_x_antes and colidiu_y_antes and (not colidiu_x_depois) and (not colidiu_y_depois)
                    q4 = colidiu_x_depois and colidiu_y_antes and (not colidiu_x_antes) and (not colidiu_y_depois)

                    # bateu em alguma quina?
                    if q1 or q2 or q3 or q4:
                        # só agora vamos usar o valor da bolinha anterior
                        
                        # se bateu em q1 ou q4 e está vindo da esquerda
                        if ((q1 or q4) and (bola.vx > 0)) or \
                           ((q2 or q3) and (bola.vx < 0)): 
                            # inverte em x
                            bola.inverte_sentido_x()  
                        else:
                            # inverte em y
                            bola.inverte_sentido_y()  
                            
                        print("quina ", end="")
                        break
                        # OBS: pra ficar perfeito, precisava ver se onde veio a bolinha
                        # para poder fazer a quicagem correta na quinta
                        # se vem da esquerda e bateu na quinta da direita precisava refletir para cima apenas
                    else:
                        # vamos ampliar o alcance de expansão dos pontos fictícios
                        # para tentar novamente
                        salto_x = salto_x + 1
                        salto_y = salto_y + 1
                        print("ampliando... ", end="")
                        # sinaliza que o salto foi ampliado
                        ampliar_salto += 1
        # se precisou ampliar o salto...                        
        if ampliar_salto > 0:
            # retira a bolinha de "dentro" do quadrado
            print("completando salto: ", ampliar_salto)
            for i in range(0,ampliar_salto):
                bola.andar()

# pega a referência temporal para poder fazer espera mais à frente
clock = pygame.time.Clock()

with app.app_context():

    '''
    # classes :-)
    class Figura:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.sentido = 1
            self.velocidade_x = 3
        def colidiu(self, outro_objeto):
            return outro_objeto.retangulo.collidepoint((self.x, self.y))
        def inverte_sentido(self):
            self.sentido *= -1
        def andar(self):
            self.x += self.velocidade_x * self.sentido

    class Retangulo(Figura):
        def __init__(self, x, y, retangulo):
            super().__init__(x, y)
            self.retangulo = retangulo
        def desenhar(self):
            pygame.draw.rect(window, color, self.retangulo) # desenho do retângulo

    class Circulo(Figura):
        def desenhar(self):
            pygame.draw.circle(window, (10, 10, 10), [self.x, self.y], 15, 5)
        
    '''

    # definição do quadradão
    rect = pygame.Rect(*window.get_rect().center, 0, 0).inflate(100, 100)
    # grandao = Retangulo(300, 300, rect)
    obstaculos = db.session.query(Retangulo).all()
    #[Retangulo(retangulo = "100,100,200,200"),
    #         Retangulo(retangulo = "320,300,100,100")]

    # definição da bolinha
    bolinhas = db.session.query(Circulo).all()
    #bolinha = Circulo(x=5, y=300)

    run = True
    while run:

        # código obrigatório para finalizar bem o programa
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False    
        
        # desenha informações na tela
        window.fill("lightgray")    
        for o in obstaculos:
            o.desenhar(window, color)
        
        for b in bolinhas:
            b.desenhar(window, color)

        # desenha os objetos efetivamente
        pygame.display.flip()


        # o circulo vazou a borda da janela?
        for b in bolinhas:
            # o circulo vazou a borda da janela?
            if b.x > X_SIZE or b.x < 0:
                b.inverte_sentido_x()
                tmp = random.randint(1,5)
                if tmp != 0:
                    if b.vx > 0:
                        b.vx = abs(tmp)
                    else:
                        b.vx = - (abs(tmp))

            if b.y > Y_SIZE or b.y < 0:    
                b.inverte_sentido_y()
           
            for o in obstaculos:
                # houve colisão?
                verifica_colisao(o, b)           
                
        # varia a posição do círculo
        for b in bolinhas:
            b.andar()
        
        # espera um pouco
        clock.tick(60)     

    pygame.quit()
    exit()