from config import *
from modelo import *

# exemplos de caverna
c1 = Caverna(continua=False, 
             mensagem="O dragão te encontrou, que pena :-(", 
             segredo = "",
             x=10, y=10,
             nome_imagem="caverna1_mini.png")

c2 = Caverna(continua=True,
             mensagem="Você encontrou o dragão da matemática, quando é 8 vezes 7? Responda com um número.",
            segredo = "56",
            x=500, y=10,
            nome_imagem="caverna2_mini.png")

c3 = Caverna(continua=True,
             mensagem="Você encontrou o dragão de biologia, o conjunto de características que recebemos de nossos pais se chama: 1) fenótipo, ou 2) genótipo?",
             segredo = "2", 
             x=300, y=300, 
             nome_imagem="caverna2_mini.png")

db.session.add(c1)
db.session.add(c2)
db.session.add(c3)
db.session.commit()

# cria um jogador
jogador1 = Jogador(nome="Jack Good", nome_imagem="cavaleiro.png")
db.session.add(jogador1)
db.session.commit()

# cria alguns exemplos de jogada
j1 = Jogada(jogador=jogador1)
j1.cavernas.append(c2)
j1.cavernas.append(c1)

db.session.add(j1)
db.session.commit()

print(c1, c2, c3)
print(j1)
print("Dados inseridos")
