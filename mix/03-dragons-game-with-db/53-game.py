from config import *
from modelo import *

import pygame

pygame.init()
screen = pygame.display.set_mode((800,600))
clock = pygame.time.Clock()

blue = (0,0,255)

# criação da fonte (apenas uma vez)
font = pygame.font.SysFont(None, 25)

# função para escrever texto na tela
def escrever_texto( janela, x, y, msg, color ):
    text = font.render( msg, True, color)
    janela.blit(text, ( x, y ) )

# obter caminho de execução deste programa
caminho = os.path.dirname(os.path.abspath(__file__))

# classe JOGADOR!
class Player(pygame.sprite.Sprite):

    # construtor
    def __init__(self, x, y, nome, nome_imagem):
        super().__init__()
        arquivo_imagem = os.path.join(caminho, nome_imagem)
        self.image = pygame.image.load(arquivo_imagem)
        self.rect = self.image.get_rect(topleft=(x, y))
        self.nome = nome
 
    # verificação de teclas
    def check_keys(self):
        # captura alguma eventual tecla que foi pressionada
        pk = pygame.key.get_pressed()
        # alguma tecla especial foi pressionada?
        if pk[pygame.K_LEFT]:
            self.rect.x -= 2
        if pk[pygame.K_RIGHT]:
            self.rect.x += 2
        if pk[pygame.K_UP]:
            self.rect.y -= 2
        if pk[pygame.K_DOWN]:
            self.rect.y += 2    

    def salvar_xy(self):
        self.antes_x = self.rect.x
        self.antes_y = self.rect.y

    def restaurar_xy(self):    
        self.rect.x = self.antes_x
        self.rect.y = self.antes_y

    # verificar se houve alguma atualização
    # na situação do jogador
    def update(self, pg):
        self.salvar_xy()
        #self.rect.y += self.y_velocity        
        self.check_keys()
        # colidiu retorna o grupo de sprites que colidiu :-)
        quem_colidiu = pygame.sprite.spritecollide(self, pg, False)
        # https://stackoverflow.com/questions/69331228/how-do-you-program-collision-in-classes/69332148#69332148
        for fig in quem_colidiu:
            escrever_texto(screen, 20, 500, fig.mensagem, blue)
        if quem_colidiu: # se alguém colidiu, essa variável vai ser avaliada como True
            # retorna para a posição anterior
            self.restaurar_xy()         
                
# classe OBSTÁCULO!
class Platform(pygame.sprite.Sprite):
    def __init__(self, x, y, nome_imagem, mensagem, segredo):
        super().__init__()
        arquivo_imagem = os.path.join(caminho, nome_imagem)
        self.image = pygame.image.load(arquivo_imagem)
        self.rect = self.image.get_rect(topleft=(x, y))
        # outros atributos persistentes
        self.mensagem = mensagem
        self.segredo = segredo


# posições iniciais do player
x = 290
y = 120

# carrega jogador do banco de dados
jogador = db.session.query(Jogador).first()

# cria o jogador em modelo de classe do jogo
player = Player(x, y, jogador.nome, jogador.nome_imagem)

# cria grupo de jogadores
player_group = pygame.sprite.Group()
# adiciona jogador no grupo :-)
player_group.add(player)

# cria grupo de obstáculos
platform_group = pygame.sprite.Group()

# carrega os obstáculos do banco
obs = db.session.query(Caverna).all()

for o in obs:
    # cria obstáculo no modelo da classe do jogo
    cav = Platform(o.x, o.y, o.nome_imagem, o.mensagem, o.segredo)
    # adiciona obstáculo no grupo
    platform_group.add(cav)

# Main game loop
running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill("white")  
    #screen.fill("lightgray")  

    # pede para cada jogador se "atualizar"
    player_group.update(platform_group)  

    escrever_texto(screen, 130, 10, "Caverna dos dragões - escolha uma caverna", blue)

    player_group.draw(screen)
    platform_group.draw(screen)
    
    # atualizar a tela
    # https://stackoverflow.com/questions/29314987/difference-between-pygame-display-update-and-pygame-display-flip
    pygame.display.update()   

    clock.tick(60)

pygame.quit()