from config import *

class Jogador(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text)
    # atributo visual: foto do jogador
    nome_imagem = db.Column(db.Text) # nome do arquivo de imagem

    def __str__(self):
        return f'{self.id}, {self.nome}, {self.nome_imagem}'
    
    def json(self):
        return {
            "id":self.id,
            "nome":self.nome,
            "nome_imagem":self.nome_imagem
        }

class Caverna(db.Model):
    # atributos da pessoa
    id = db.Column(db.Integer, primary_key=True)
    mensagem = db.Column(db.Text)
    continua = db.Column(db.Boolean)
    segredo = db.Column(db.Text)
    # atributos visuais
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    nome_imagem = db.Column(db.Text) 

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f'''{self.id}, {self.mensagem}, {self.continua}, {self.segredo}
        {self.x}, {self.y}, {self.nome_imagem}
               '''
    def json(self):
        return {
            "id":self.id,
            "mensagem":self.mensagem,
            "continua":self.continua,
            "segredo":self.segredo,
            "x":self.x,
            "y":self.y,
            "nome_imagem":self.nome_imagem
        }

class Jogada(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    jogador_id = db.Column(db.Integer, db.ForeignKey(Jogador.id), nullable=False)
    jogador = db.relationship("Jogador")

    # lista de cavernas :-)
    cavernas = db.relationship("Caverna", secondary="cavernaDaJogada")
    
    def __str__(self):
        s = f'''
        Jogador: {self.jogador}
        '''
        for c in self.cavernas:
            s += f'\n passou em: {c}'
        
        return s

# tabela n x n (sem classe - acessível via lista "cavernas" na classe "Jogada")
cavernaDaJogada = db.Table('cavernaDaJogada', db.metadata,
    db.Column('id_caverna', db.Integer, db.ForeignKey(Caverna.id)),
    db.Column('id_jogada', db.Integer, db.ForeignKey(Jogada.id))
)