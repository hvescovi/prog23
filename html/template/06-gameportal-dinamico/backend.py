from flask import Flask, jsonify
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

@app.route("/")
def padrao():
    return "backend ok"

@app.route("/games")
def produtos():
    return jsonify({"resultado":"ok",
                   "detalhes":[
                       {"titulo":"Monstrinho",
                        "descricao":"Um jogo assustador"},
                        {"titulo":"Fofura",
                        "descricao":"Joguinho bonitinho"},
                        {"titulo":"Estrela",
                        "descricao":"Vá para o espaço!"},
                        {"titulo":"Escolástico",
                        "descricao":"Jogo educativo!"}
                        ]})

app.run(debug=True)