from flask import Flask, jsonify
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

@app.route("/")
def padrao():
    return "backend ok"

@app.route("/produtos")
def produtos():
    return jsonify({"resultado":"ok",
                   "detalhes":[
                       {"descricao":"cookies",
                        "preco":30},
                        {"descricao":"baunilha",
                        "preco":20},
                        {"descricao":"amêndoa",
                        "preco":50},
                        {"descricao":"polvilho",
                        "preco":18}
                        ]})

app.run(debug=True)