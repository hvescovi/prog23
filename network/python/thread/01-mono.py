# https://realpython.com/intro-to-python-threading/#working-with-many-threads
import threading
import time

def func(name):
    print("func", name, "rodando")
    time.sleep(2)

x = threading.Thread(target=func, args=(1,))
x.start()