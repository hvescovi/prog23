@startuml

start

:Mostrar introdução;

-> continua = True;

while (continua?) is (True)

  :Jogador escolhe uma caverna;
  if (dragão amigável) then (sim)
    :Apresenta o desafio;
    if (jogador acertou) then (sim)
      :Parabeniza o jogador;
    else (não)
      :Informa quantas vezes o jogador escapou;
      :Informa que o jogador foi devorado;
      -> continua = False;
    endif
  else (não)
    :Informa quantas vezes o jogador escapou;
    :Informa que o jogador foi devorado;
    -> continua = False;
  endif

endwhile (False)

stop

@enduml
