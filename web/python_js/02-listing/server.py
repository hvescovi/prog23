# importar a biblioteca flask
from flask import Flask, jsonify
# biblioteca CORS
from flask_cors import CORS
# importar a classe Pessoa
from pessoa import *

# acesso ao flask via variável app
app = Flask(__name__)

# inserindo a aplicação em um contexto
# https://flask.palletsprojects.com/en/2.2.x/appcontext
with app.app_context():

    # aplicando tratamento CORS ao flask
    # https://flask-cors.readthedocs.io/en/latest/
    CORS(app)

    # rota padrão
    @app.route("/")
    def ola():
        return "<b>Olá, gente!</b>"

    # rota de listar pessoas
    @app.route("/listar_pessoas")
    def listar():
        try:
            # criar uma lista vazia para retorno das informações
            lista_retorno = []
            # criar uma lista de pessoas
            lista = [
                Pessoa(nome="João da Silva",
                      email="josilva@gmail.com",
                      telefone="47 99012 3232"),
                Pessoa(nome="Maria Oliveira",
                      email="maliva@gmail.com",
                      telefone="47 98823 4321"),
                Pessoa(nome="Teresa Soares",
                      email="teso@gmail.com",
                      telefone="47 98114 1423"),
            ]
            # percorrer a lista de pessoas
            for p in lista:
                # adicionar na lista de retorno a pessoa em formato json
                lista_retorno.append(p.json())

            # preparar uma parte da resposta: resultado ok
            meujson = {"resultado":"ok"}
            # preparar a outra parte da resposta: os resultados em si
            # o comando abaixo atualiza um dicionário, fazendo um "merge" entre eles
            meujson.update({"detalhes":lista_retorno})
            # retornar a lista de pessoas json, com resultado ok
            resposta = meujson
            #x = 1 /0 # simular erro de divisão por zero
            # é preciso remover o debug=True para que o servidor
            # trate corretamente esse erro
        except Exception as e: 
            resposta = jsonify({"resultado": "erro", "detalhes": str(e)})
    
        
        # talvez seja preciso REMOVER debug=True PARA OBSERVAR OS ERROS ABAIXO
        #return jsonify([{"numero":"123"}]) 
        #return jsonify({"numero":"123"})  
        #return "{123}" # falha ao decodificar json, não está no formato {"chave":valor}

        return resposta

    app.run(debug=True)

    # para depurar a aplicação web no VSCode, é preciso remover debug=True
    # https://stackoverflow.com/questions/17309889/how-to-debug-a-flask-app

'''
resultado da invocação ao servidor:

curl localhost:5000/listar_pessoas


'''
