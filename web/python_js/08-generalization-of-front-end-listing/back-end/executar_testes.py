from geral.config import *
from testes import *

# inserindo a aplicação em um contexto :-/
with app.app_context():

    TestarPessoa.run()
    TestarRespirador.run()
    TestarExameRealizado.run()
