from geral.config import *

from modelo.pessoa import *
from modelo.exame import *
from modelo.exame_realizado import *
from modelo.respirador import *

# inserindo a aplicação em um contexto :-/
with app.app_context():

    if os.path.exists(arquivobd):
        os.remove(arquivobd)

    # criar tabelas
    db.create_all()

    print("Banco de dados e tabelas criadas")