from flask import Flask

app = Flask(__name__)

# contador de execuções da rota "operacao"
cont = 0

@app.route("/")
def ola():
    return "<b>Olá, mundo!</b>"

@app.route("/operacao")
def operacao():
    global cont # declaração de variável global
    cont += 1
    print("contador: ", cont)
    return "A operação foi executada, e o contador foi incrementado"

@app.route("/contador")
def contador():
    return str(cont)

app.run(debug=True)