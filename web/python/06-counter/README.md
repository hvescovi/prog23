Para usar um contador no servidor web:

1) inicializa a variável
cont = 0

2) dentro da rota:

a) declara o acesso à variável externa/global:
global cont

b) utiliza a variável:
cont += 1

TESTAR o contador:

1) instalar apache benchmark:
sudo apt install apache2-utils

2) 1000 requests, 10 clientes simultâneos:
ab -n 1000 -c 10 http://localhost:5000/operacao

