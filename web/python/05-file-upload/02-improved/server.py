# https://stackoverflow.com/questions/19340654/python-script-that-uploads-a-file-without-using-a-form-and-just-uses-flask-as-ap
from flask import Flask
from flask import request
app = Flask(__name__)
import os

@app.route('/')
def hello_world():
    return 'Hello World!'

# curl -i -X POST -F files=@imagem1.png http://127.0.0.1:5000/upload
# curl -i -X POST -F files="@pdf1 14pgs.pdf" http://127.0.0.1:5000/upload
@app.route('/upload', methods= ['POST'])
def upload_file():
    f = request.files['files']
    # print(f.filename)
    caminho = os.path.dirname(os.path.abspath(__file__))
    com_pasta = os.path.join(caminho, 'files/')
    completo = os.path.join(com_pasta, f.filename)
    f.save(completo)
    return '200'
    
app.debug = True
app.run()