# https://stackoverflow.com/questions/19340654/python-script-that-uploads-a-file-without-using-a-form-and-just-uses-flask-as-ap
from flask import Flask
from flask import request
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

# curl -i -X POST -F files=@meuteste.txt http://127.0.0.1:5000/upload
@app.route('/upload', methods= ['POST'])
def upload_file():
    # obtém o valor do parâmetro 'files'
    # -F no curl emula o envio de um formulário
    f = request.files['files']
    # obtém o caminho do arquivo atual
    caminho = os.path.dirname(os.path.abspath(__file__))
    # acrescenta o nome de arquivo
    novo = os.path.join(caminho, 'files/test.txt')
    # salva o conteúdo 
    f.save(novo)
    # resposta status 200 http (ok)
    # tabela de respostas padronizadas:
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
    return '200'

# iniciar o servidor
app.debug = True
app.run()