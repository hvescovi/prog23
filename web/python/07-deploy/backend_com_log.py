from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
import os
# https://dev.to/thetrebelcc/how-to-run-a-flask-app-over-https-using-waitress-and-nginx-2020-235c
from waitress import serve
# https://python-forum.io/thread-33422.html
from paste.translogger import TransLogger

import logging.config
logging.basicConfig(
    filename='meuslogs.log', 
    level=logging.INFO
)
# níveis de debug: 
# https://docs.python.org/3/library/logging.html#logging-levels

path = os.path.dirname(os.path.abspath(__file__)) 
arquivobd = os.path.join(path, 'pessoa.db')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Pessoa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    email = db.Column(db.String(254))
    telefone = db.Column(db.String(254))

    def __str__(self):
        return self.nome + "[id="+str(self.id)+ "], " +\
            self.email + ", " + self.telefone

    def json(self):
        return { "id": self.id, 
                 "nome": self.nome, 
                 "email": self.email, 
                 "telefone": self.telefone
               }

@app.route("/")
def ola():
    return "Servidor backend operante"

@app.route("/listar")
def listar():
    pessoas = db.session.query(Pessoa).all()
    lista = []
    for p in pessoas:
        lista.append(p.json())
    resposta = jsonify(lista)
    return resposta

#app.run(debug=True)
#serve(app, host='0.0.0.0', port=5000)
serve(TransLogger(app, setup_console_handler=False), host='0.0.0.0', port=5000)

# teste: curl localhost:5000/listar