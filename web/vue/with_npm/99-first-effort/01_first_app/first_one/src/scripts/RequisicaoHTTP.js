export default {
  data() {
    return {
      products: [],
      error: null
    };
  },
  methods: {
    getProducts: function () {
      fetch('https://reqres.in/api/products')
        .then(response => response.json())
        .then(json => {
          this.products = json.data;
        })
        .catch(error => {
          this.error = error;
        });
    }
  }
};