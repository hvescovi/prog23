export default {
  data() {
    return {
      pessoas: [],
      error: null
    };
  },
  methods: {
    getPessoas: function () {
      fetch('http://localhost:5000/listar_pessoas')
        .then(response => response.json())
        .then(json => {
          //console.log(json);
          if (json.resultado == 'ok') {
            this.pessoas = json.detalhes;
          } else {
            // está mascarando o erro, melhorar essa parte depois
            this.pessoas = []
          }
        })
        .catch(error => {
          this.error = error;
        });
    }
  }
};