import axios from 'axios';

export default {
  data() {
    return {
      dados: {
        nome: 'João da Silva Oliveira',
        email: 'josilvaliv@gmail.com',
        telefone: '47 91234 5678'
      },
      mensagem: "",
    }
  },
  methods: {
    incluirPessoa() {
      axios.post('http://localhost:5000/incluir_pessoa',
        this.dados)
        .then(response => {
          //console.log(response);
          if (response.data.resultado == 'ok') {

            // informa sucesso na operação
            this.mensagem = "Pessoa incluída com sucesso!";
            // limpa os campos
            this.nome = "";
            this.email = "";
            this.telefone = "";
          } else {
            // exibe mensagem de erro
            this.mensagem = response.data.detalhes;
          }
        })
        //response.json())
        //.then(data => console.log(data))
        /*
        .then(json => {
          if (json.resultado == 'ok') {

            // informa sucesso na operação
            this.mensagem = "Pessoa incluída com sucesso!";
            // limpa os campos
            this.nome = "";
            this.email = "";
            this.telefone = "";
          } else {
            // exibe mensagem de erro
            this.mensagem = json.detalhes;
          }
        })*/
        .catch(error => {
          this.mensagem = error;
        });
    }
  }
};