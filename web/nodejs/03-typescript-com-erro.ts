console.log("vamos lá");

type User = {
  name: string;
  age: number;
};

function isAdult(user: User): boolean {
  return user.age >= 18;
}

const justine: User = {
  name: 'Justine',
  age: 'Segredo', // ERROOOOO idade era pra ser número !!!!!!
};

// uso da função
const isJustineAnAdult: boolean = isAdult(justine);

console.log("nada certo :-( esse programa tem texto em lugar de número!", isJustineAnAdult);

/*

- ao tentar executar, será informado o erro de texto 
em lugar de número :-)

mas se tentar executar o arquivo javascript, 
ele vai de boa (javascript ripongão deixa tudo fluir, 
acontece naturalmente, pra quê mostrar erro?):

$ node 03-typescript-com-erro.js 
vamos lá
nada certo :-( esse programa tem texto em lugar de número! false

*/