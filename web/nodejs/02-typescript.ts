console.log("vamos lá");

// "classe" usuário
type User = {
  name: string;
  age: number;
};

// observar o parâmetro da função: tipado :-)
// o retorno também é tipado
function isAdult(user: User): boolean {
  return user.age >= 18;
}

// "instância"
const justine: User = {
  name: 'Justine',
  age: 23,
};

// uso da função
const isJustineAnAdult: boolean = isAdult(justine);

console.log("tudo certo :-)", isJustineAnAdult);

/*

https://nodejs.dev/en/learn/nodejs-with-typescript/

instalar typescript:
$ npm i -D typescript

- após a execução desse comando, serão gerados dois arquivos 
e uma pasta com vários conteúdos ("bibliotecas" do typescript)

executar este programa exemplo:
$ npx tsc 02-typescript.ts

- será gerado um arquivo 02-typescript.js para ser executado pelo node

para ver as saídas do console.log, execute o arquivo .js:
$ node 02-typescript.js

*/