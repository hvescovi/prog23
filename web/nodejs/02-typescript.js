console.log("vamos lá");
// observar o parâmetro da função: tipado :-)
// o retorno também é tipado
function isAdult(user) {
    return user.age >= 18;
}
// "instância"
var justine = {
    name: 'Justine',
    age: 23
};
// uso da função
var isJustineAnAdult = isAdult(justine);
console.log("tudo certo :-)", isJustineAnAdult);
/*

instalar typescript:
$ npm i -D typescript

- após a execução desse comando, serão gerados dois arquivos
e uma pasta com vários conteúdos ("bibliotecas" do typescript)

executar este programa exemplo:
$ npx tsc 02-typescript.ts

- será gerado um arquivo 02-typescript.js para ser executado pelo node

*/ 
