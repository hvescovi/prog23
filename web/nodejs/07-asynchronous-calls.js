/*
  difícil para ler e depurar
  suscetível a erros
*/

function doStep1(init, callback) {
    console.log("step1");
    const result = init + 1;
    callback(result);
}

function doStep2(init, callback) {
    console.log("step2");
    const result = init + 2;
    callback(result);
}

function doStep3(init, callback) {
    console.log("step3");
    const result = init + 3;
    callback(result);
}

function doOperation() {
    doStep1(0, (result1) => {
        doStep2(result1, (result2) => {
            doStep3(result2, (result3) => {
                console.log(`result: ${result3}`);
            });
        });
    });
}

doOperation();
