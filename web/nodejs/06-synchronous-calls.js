// node 06-synchronous-call.js

function doStep1(init) {
  console.log("step1");
  return init + 1;
}

function doStep2(init) {
  console.log("step2");
  return init + 2;
}

function doStep3(init) {
  console.log("step3");
  return init + 3;
}

function doOperation() {
  let result = 0;
  result = doStep1(result);
  result = doStep2(result);
  result = doStep3(result);
  console.log(`result: ${result}`);
}

doOperation();
