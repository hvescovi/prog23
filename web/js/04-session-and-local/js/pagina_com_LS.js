$(function () { // quando o documento estiver pronto/carregado

    // tenta obter o login
    var login = localStorage.getItem("login")

    // não está logado?
    if (login == null) {
        // deixa o formulário de login visível
        $("#formulario").removeClass("d-none");
        // mensagem padrão
        var mensagem = "Faça login para acessar os recursos do sistema.";
    } else {
        // altera a mensagem para boas vindas :-)
        var mensagem = "Bem vindo, " + login;
        // exibe a tela de logout
        $("#logout").removeClass("d-none");
        // exibe o botão de logout
        $("#btLogout").removeClass("d-none");
        // exibe a tabela
        $("#tabela").removeClass("d-none");
    }

    // coloca a mensagem na tela
    $("#mensagem").text(mensagem);

    // console.log("login: ",login);

    // código para mapear click do botão incluir pessoa
    $(document).on("click", "#btLogin", function () {
        //pegar dados da tela
        login = $("#campoLogin").val();
        senha = $("#campoSenha").val();

        if (login == "mylogin" && senha == "123") {
            // guarda na sessao de forma mais permanente
            localStorage.setItem('login', login);

            // atualiza a página
            window.location = 'pagina_com_local_storage.html';
        } else {
            alert("Login ou senha inválido(s)!!");
        }
    });

    // código para mapear click do botão incluir pessoa
    $(document).on("click", "#btLogout", function () {
        // limpar o login do storage
        localStorage.removeItem("login");

        // atualiza a página
        window.location = 'pagina_com_local_storage.html';
    });

}); 