$(function () {

    // obtém login da sessão
    var login = sessionStorage.getItem('login');
    
    // o login existe?
    if (login == null) {
        $("#menu").html(`Você ainda não fez login. 
                        <a href=form_login.html>Faça agora</a> :-)`);
    } else {
        // carrega o menu de opções
        // observar uso de acento de crase para 
        // poder inserir HTML livremente

        $("#menu").html(`
        
        Bem vindo, <b>${login}</b> | <a href="principal.html">Início</a> | 
        <a href="listar.html">Listar</a> |
        Incluir |
        Opções |
        <a href=# id="linkLogout">Logout</a>
                
        `);        
    }
    
    // código para mapear click do botão de logout
    $(document).on("click", "#linkLogout", function () {
        // remove item da sessao
        sessionStorage.removeItem('login');
        // atualiza a tela
        window.location = 'principal.html';
    });
});