# importações
# ***********
# pip3 install flask
# pip3 install flask_sqlalchemy
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# configurações
# *************
# vínculo com o Flask
app = Flask(__name__) 
# sqlalchemy usando sqlite: obter o caminho do arquivo atual
caminho = os.path.dirname(os.path.abspath(__file__))
# concatenar o caminho com o nome do arquivo de banco de dados
arquivobd = os.path.join(caminho, 'pessoas.db')
# configurar o caminho completo
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + arquivobd
# vínculo com o SQLAlchemy
db = SQLAlchemy(app) 

# criando contexto para a aplicação :-/
with app.app_context():

    # classe
    # ******
    class Resultado(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        nome_jogador_1 = db.Column(db.Text)
        nome_jogador_2 = db.Column(db.Text)
        # 1 = jogador 1 ganhou, 2 = jogador 2 ganhou
        quem_ganhou = db.Column(db.Integer)

        # método para expressar o resultado em forma de texto
        def __str__(self):
            return f'(id={self.id}) {self.nome_jogador_1}, '+\
                f'{self.nome_jogador_2}, {self.quem_ganhou}'

    # teste da classe
    # ***************

    # criar tabelas
    # se alterar a classe, precisa apagar o arquivo para
    # que as tabelas sejam criadas novamente!
    db.create_all()

    # criar instância da classe, com valores
    r1 = Resultado(nome_jogador_1 = "João", nome_jogador_2 = "Maria", quem_ganhou = 1)
    
    # persistir 
    db.session.add(r1)
    db.session.commit()

    # exibir
    print(r1)