# importações
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# flask
app = Flask(__name__)

# OPÇÃO 2: sqlalchemy com mysql ------------------
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://ifc_bd2_2023:bd_2023@191.52.7.126/ifc_bd2_2023"

# configurações comuns para opção 1 e 2
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)