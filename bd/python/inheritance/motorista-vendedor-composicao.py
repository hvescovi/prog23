# importações
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# configurações
app = Flask(__name__)
path = os.path.dirname(os.path.abspath(__file__))
arquivobd = os.path.join(path, 'profissoes.db')
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # remover warnings
db = SQLAlchemy(app)

# definições das classes
class Pessoa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    telefone = db.Column(db.String(254))
    email = db.Column(db.String(254))
    def __str__(self):
        return f'{self.nome}, {self.telefone}, {self.email}'

class Vendedor(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    # quem é a pessoa desse vendedor?
    pessoa_id = db.Column(db.Integer, db.ForeignKey('pessoa.id'), nullable=False)
    pessoa = db.relationship(Pessoa)
    
    comissao = db.Column(db.Float) # atributo do vendedor
    def __str__(self):
        return f"Pessoa: {self.pessoa}, comissão={self.comissao}"

class Motorista(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    pessoa_id = db.Column(db.Integer, db.ForeignKey('pessoa.id'), nullable=False)
    pessoa = db.relationship(Pessoa)
    
    cnh = db.Column(db.String(254)) # atributo do motorista
    def __str__(self):
        return "Pessoa: {self.pessoa}, comissão={self.cnh}"

# criando contexto para a aplicação :-/
with app.app_context():

    # início do programa de testes
    if os.path.exists(arquivobd): # se o arquivo já existe...
        os.remove(arquivobd) # ... o arquivo é removido

    db.create_all() # criar as tabelas no banco

    p1 = Pessoa(nome="João da Silva", telefone="",
                email="josilva@gmail.com")
    v1 = Vendedor(pessoa=p1, comissao=10) # João, 10%
    #v1.pessoa = p1
    #v1.comissao = 10
    db.session.add(p1)
    db.session.add(v1) 
    db.session.commit()

    print(f'Vendedor: {v1}')
    
    p2 = Pessoa(nome="Maria Oliveira", email="mari@gmail.com", 
                telefone="98813-1415")
    m1 = Motorista(pessoa=p2, cnh="123-4")
    # Motorista: Maria Oliveira, 98813-1415, mari@gmail.com, CNH: 123-4
    db.session.add(m1)
    db.session.commit()
    print(f'Motorista: {m1}')
    
    # listando as pessoas :-)
    print("Listando pessoas:")
    for p in db.session.query(Pessoa).all():
        print(p)
    '''
    Listando pessoas:
    '''

    # Maria começou a vender também - tudo certo :-)
    v2 = Vendedor(pessoa=p2, comissao = 15)
    print(f'Agora é vendedora :-p {v2}')
    db.session.add(v2)
    db.session.commit()

    # tudo certinho no bd ;-p