import psycopg2
import time

# precisou instalar este pacote para o pip3 instalar o psycopg2:
# sudo apt install libpq-dev
# https://stackoverflow.com/questions/11618898/pg-config-executable-not-found

import threading

# configurações
mydb = psycopg2.connect(
  host="191.52.6.76",
  user="postgres",
  password="root",
  database="chicago"
)
# conexão
cursor = mydb.cursor()

def lista(q):
    # select
    cursor.execute(f"SELECT * from chicago limit "+str(q))
    lista = cursor.fetchall()
    print('task ',q)
    for linha in lista:
      print(linha[0])
    #time.sleep(1)

# número de tarefas simultâneas
n = 5

tasks = []
# criando as tasks
for i in range(n):
    t = threading.Thread(target=lista, args=(i,))
    tasks.append(t)

# executando
for t in tasks:
  t.start()

# aguardando todas terminarem
for t in tasks:
  t.join()

print("todas terminaram")

'''
friend@friend-HP-640-22:~$ /bin/python3 /home/friend/02-gitlab/prog22/bd/py/ad_postgres_multithreading.py
task  1
AARON,  JEFFERY M
task  3
AARON,  JEFFERY M
AARON,  KARINA
AARON,  KIMBERLEI R
task  0
task  4
task  2
AARON,  JEFFERY M
AARON,  KARINA
AARON,  KIMBERLEI R
ABAD JR,  VICENTE M
todas terminaram
'''