import mysql.connector

# configurações
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="testando",
  database="chicago"
)

# conexão
mycursor = mydb.cursor()

# comando select
mycursor.execute("SELECT * FROM Current_Employee_Names where Department = 'POLICE'")

# obter os resultados
myresult = mycursor.fetchall()

# percorrer os resultados
for x in myresult:
  print(x)

# referência:
# https://www.w3schools.com/python/python_mysql_select.asp

# referências agosto de 2023 (ERRO NOVO >-o)
# export MYSQLCLIENT_CFLAGS="-I"
# https://stackoverflow.com/questions/76875507/specify-mysqlclient-cflags-and-mysqlclient-ldflags-env-vars-manually-while-pip-i
# $ export MYSQLCLIENT_LDFLAGS="-L"
# $ export MYSQLCLIENT_CFLAGS="-I"

# instalei:
# sudo apt install python3-mysql.connector

# contribuição do Gabriel Sá:
# nas máquinas do lab A04, usar:
# pip3 install mysql-connector-python