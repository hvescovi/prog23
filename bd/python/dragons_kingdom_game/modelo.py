from config import *

class Caverna(db.Model):
    # atributos da pessoa
    id = db.Column(db.Integer, primary_key=True)
    mensagem = db.Column(db.Text)
    continua = db.Column(db.Boolean)
    segredo = db.Column(db.Text)

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f'(id={self.id}) {self.mensagem}, '+\
               f'{self.continua}, {self.segredo}'

class Jogada(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome_jogador = db.Column(db.Text)

    # lista de cavernas :-)
    cavernas = db.relationship("Caverna", secondary="cavernaDaJogada")
    
    def __str__(self):
        s = f'''
        Jogador: {self.nome_jogador}
        '''
        for c in self.cavernas:
            s += f'\n passou em: {c}'
        
        return s

# tabela n x n (sem classe - acessível via lista "cavernas" na classe "Jogada")
cavernaDaJogada = db.Table('cavernaDaJogada', db.metadata,
    db.Column('id_caverna', db.Integer, db.ForeignKey(Caverna.id)),
    db.Column('id_jogada', db.Integer, db.ForeignKey(Jogada.id))
)