from config import *
from modelo import *

# exemplos de caverna
c1 = Caverna(continua=False, 
             mensagem="O dragão te encontrou, que pena :-(", 
             segredo = "")

c2 = Caverna(continua=True,
             mensagem="Você encontrou o dragão da matemática, quando é 1+1? Responda com um número.",
            segredo = "2")

c3 = Caverna(continua=True,
             mensagem="Você encontrou o dragão de biologia, o conjunto de características que recebemos de nossos pais se chama: 1) fenótipo, ou 2) genótipo?",
             segredo = "2")

db.session.add(c1)
db.session.add(c2)
db.session.add(c3)
db.session.commit()

# cria alguns exemplos de jogada
j1 = Jogada(nome_jogador="Hylson")
j1.cavernas.append(c1)
j1.cavernas.append(c2)

db.session.add(j1)
db.session.commit()

print(c1, c2, c3)
print(j1)
print("Dados inseridos")
