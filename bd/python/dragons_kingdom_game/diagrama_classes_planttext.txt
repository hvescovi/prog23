@startuml

class Caverna {
  mensagem: String
  continua: Boolean
  segredo: String
}
class SequenciaJogada {
  data: String
  nome_jogador: String
  jogadas: List of Caverna
}

note right
A última caverna 
é aquela na qual 
o jogador perdeu
(a sequência importa)
end note

SequenciaJogada *-- Caverna

@enduml
