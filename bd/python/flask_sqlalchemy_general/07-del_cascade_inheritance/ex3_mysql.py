#
# necessário para acesso python > mysql:
#
# sudo apt install python3-mysqldb
#
# pip3 install pymysql

# importações
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# docker run --name=test-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=testando -d -e MYSQL_ROOT_HOST=% mysql/mysql-server:5.7
# docker run --name myadmin -d --link test-mysql:db -p 8080:80 phpmyadmin/phpmyadmin

# configurações
app = Flask(__name__)
#path = os.path.dirname(os.path.abspath(__file__)) # sugestao do Kaue
#arquivobd = os.path.join(path, 'ex2.db')
# mysql: conectar com 127.0.0.1
# https://stackoverflow.com/questions/18150858/operationalerror-2002-cant-connect-to-local-mysql-server-through-socket-v
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:testando@127.0.0.1:3306/legal'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:testando@127.0.0.1:3306/hylson_poo2'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # remover warnings
db = SQLAlchemy(app)

# comando mágico necessário a partir do python 10
app.app_context().push()

class Pai(db.Model):

    id = db.Column(
        db.Integer, 
        db.Sequence('pai_id_seq', start=1001, increment=1),
        primary_key=True
    )
    nome = db.Column(db.String(50))
    discriminator = db.Column('type', db.String(20), nullable=False)

    __tablename__ = 'pai'
    __mapper_args__ = {
        'polymorphic_on': discriminator,
        'polymorphic_identity': 'pai',
    }
    def __str__(self):
        return str(self.id)+", "+self.nome

class Filho(Pai):
    id = db.Column(db.Integer, db.ForeignKey('pai.id', ondelete="CASCADE"), primary_key=True)

    tamanho_bar = db.Column(db.String(10))

    __tablename__ = 'filho'
    __mapper_args__ = {
        'polymorphic_identity': 'filho',
    }
    def __str__(self):
        return str(self.id)+", "+self.nome+", "+self.tamanho_bar


# início do programa de testes
#if os.path.exists(arquivobd): # se o arquivo já existe...
#    os.remove(arquivobd) # ... o arquivo é removido

#db.session.execute("PRAGMA foreign_keys=ON");
db.create_all() # criar as tabelas no banco

p1 = Pai(nome = "primeiro pai")
db.session.add(p1)
db.session.commit()

p2 = Pai(nome = "segundo pai")
db.session.add(p2)
db.session.commit()

f1 = Filho(nome = "filho 3", tamanho_bar = "cinquenta")
db.session.add(f1)
db.session.commit()

print("AAA - listando pais e filhos")
for p in db.session.query(Pai).all():
    print("pai: ", p)   
for f in db.session.query(Filho).all():
    print("filho: ", f)

db.session.query(Pai).filter(Pai.id==1).delete()

print("BBB - listagem após exclusão do pai 1")
for p in db.session.query(Pai).all():
    print("pai: ", p)   
for f in db.session.query(Filho).all():
    print("filho: ", f)

db.session.query(Pai).filter(Pai.id==3).delete()

print("CCC - listagem após exclusão do pai três - exclusão CASCADE")
for p in db.session.query(Pai).all():
    print("pai: ", p)   
for f in db.session.query(Filho).all():
    print("filho: ", f)

db.session.commit()