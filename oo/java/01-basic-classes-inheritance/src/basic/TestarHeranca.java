package basic;

import java.util.ArrayList;

public class TestarHeranca {

    public static void main(String[] args) {

        Pessoa p1 = new Pessoa();
        p1.nome = "João";
        p1.email = "josilva@gmail.com";
        p1.dt_nasc = "01/02/2000";
        System.out.println(p1.nome);

        // pessoa não tem telefone
        // tentar mostrar telefone da pessoa? nem compila
        // System.out.println(p1.telefone);
        Paciente pa1 = new Paciente();
        pa1.nome = "Maria";
        pa1.email = "ma@gmail.com";
        pa1.dt_nasc = "01/01/2018";
        pa1.telefone = "81818181";
        pa1.tps = "O+";
        System.out.println(pa1.telefone);

        // converter paciente para pessoa
        Pessoa p2 = (Pessoa) pa1;

        // também não deixa mostrar telefone da pessoa
        // o objeto Paciente está criado na memória, mas...
        // ... a variável-ponteiro "p2" não consegue acessar o telefone
        //System.out.println(p2.telefone);
        // converter novamente pessoa para paciente
        // na verdade, vamos criar outro ponteiro para apontar o mesmo objeto
        Paciente pa3 = (Paciente) p2;

        System.out.println("E agora, tem telefone?");
        System.out.println(pa3.telefone);

        // vamos criar um médico
        Medico m1 = new Medico();
        m1.nome = "Erick Krueger";
        m1.email = "ekru@gmail.com";
        m1.dt_nasc = "10/02/1996";
        m1.crm = "10293847";
        m1.especialidade = "homeopatia";

        // fazendo uma lista com diferentes tipos de pessoa
        ArrayList<Pessoa> pessoas = new ArrayList();
        pessoas.add(pa3);
        pessoas.add(m1);
        pessoas.add(p1);

        // percorrer a lista
        for (Pessoa p : pessoas) {
            System.out.print("Nome: " + p.nome);
            if (p instanceof Paciente) {
                System.out.print(" - paciente, tps = ");
                System.out.println(((Paciente) p).tps);
            } else if (p instanceof Medico) {
                System.out.print(" - medico, crm = ");
                System.out.println(((Medico) p).crm);
            } else {
                System.out.println("Essa pessoa é... só uma pessoa");
            }
        }
    }
}
