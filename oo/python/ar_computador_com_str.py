class Computador:
    def __init__(self, tipo, marca, so):
        self.tipo = tipo # notebook ou desktop
        self.marca = marca
        self.sistema_operacional = so
    def __str__(self):
        return f'''
    Este computador é um {self.tipo} da marca 
    {self.marca} e possui instalado: {self.sistema_operacional}'''
        
c1 = Computador("Notebook", "Asus", "Windows")
print(c1)

c2 = Computador("Desktop", "eu que montei", "Linux")
print(c2)
