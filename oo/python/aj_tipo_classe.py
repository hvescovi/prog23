class Pessoa:
    def __init__(self, nome, email, dt_nasc):
        self.nome = nome
        self.email = email
        self.data_nascimento= dt_nasc
class Paciente(Pessoa):
    def __init__(self, nome, email, dt_nasc, telefone, tps):
        super().__init__(nome, email, dt_nasc)
        self.telefone = telefone
        self.tipo_sanguineo = tps        

# dados de teste
nome = "João da Silva"
email = "josilva@gmail.com"
telefone = "47 91234-5678"
# criar a pessoa
nova_pessoa = Pessoa(nome, email, telefone)
# exibir os dados
print(nova_pessoa.nome)

# teste do paciente
nome = "Maria Oliveira"
email = "maliv@gmail.com"
nasc = "18/02/1978"
telefone = "944901221"
tipo_sang = "AB-+"
# criar a paciente
nova_paciente = Paciente(nome, email, nasc, telefone, tipo_sang)
# exibir os dados
print("Paciente:", nova_paciente.nome)

# realizando um teste de tipo
print("teste 1")
# variável para fazer o teste
teste = nova_pessoa
# esse objeto é do tipo Pessoa?
if isinstance(teste, Pessoa):
    print("Encontrei um objeto do tipo Pessoa :-)")
if isinstance(teste, Paciente):
    print("Encontrei um objeto do tipo Paciente :-)")
# exibindo o tipo diretamente
print("Afinal, qual é o tipo deste objeto?", type(teste))

# realizando outro teste de tipo
print("teste 2")
# vamos testar agora um objeto do tipo Paciente
teste = nova_paciente
# esse objeto é do tipo Pessoa?
print("teste 2")
if isinstance(teste, Pessoa):
    print("Encontrei um objeto do tipo Pessoa :-)")
if isinstance(teste, Paciente):
    print("Encontrei um objeto do tipo Paciente :-)")
print("Tipo do objeto?", type(teste))

# pessoa não tem atributo telefone?
try:
    print("Pessoa tem atributo telefone, viva, mágica!", nova_pessoa.telefone)
except:
    print("É, Pessoa não tem atributo telefone")

# um paciente pode virar pessoa?
alguem_pessoa = nova_paciente
# conversão "fraca" de tipo
alguem_pessoa.__class__ = Pessoa
# este atributo vai existir
print(alguem_pessoa.nome)
try:
    # este atributo deveria dar erro, mas no python não dá
    print("Eita, o telefone existe??", alguem_pessoa.telefone) 
except:
    print("Oba, deu erro :-)")
