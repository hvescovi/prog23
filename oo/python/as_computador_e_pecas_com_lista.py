class Computador:
    def __init__(self, tipo, marca, so):
        self.tipo = tipo # notebook ou desktop
        self.marca = marca
        self.sistema_operacional = so
        self.pecas = []
    def __str__(self):
        # variável acumuladora
        string_pecas = ""
        # percorrer a lista de pecas
        for p in self.pecas:
            # acumula a peça na string
            string_pecas += "|" + str(p) + "|"

        return f'''
    Este computador é um {self.tipo} da marca 
    {self.marca} e possui instalado: {self.sistema_operacional}.
    Peças instaladas: {string_pecas}'''
        
c1 = Computador("Notebook", "Asus", "Windows")
print(c1)
c2 = Computador("Desktop", "eu que montei", "Linux")
print(c2)

class Peca:
    def __init__(self, descricao, marca, preco):
        self.descricao = descricao
        self.marca = marca
        self.preco = preco
    def __str__(self):
        return f'''
        Descrição: {self.descricao},
        Marca: {self.marca},
        Preço: {self.preco}'''

p1 = Peca("Placa de Video 8Gb", "Afox", 1500)
p2 = Peca("HD SSD 240GB", "Kingston", 400)
p3 = Peca("Processador", "Intel i5", 1300)

print(p1)

c1.pecas.append(p1)
c1.pecas.append(p2)
c1.pecas.append(p3)

print(c1)