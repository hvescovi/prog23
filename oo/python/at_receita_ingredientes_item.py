class Receita:
    def __init__(self, nome, modo_preparo, referencia):
        self.nome = nome
        self.modo_preparo = modo_preparo
        self.referencia = referencia
    def __str__(self):
        return f'''
        Receita: {self.nome},
        Modo de preparo: {self.modo_preparo},
        Referência: {self.referencia},
        '''
    
class Ingrediente:
    def __init__(self, nome, preco_medio_por_kg):
        self.nome = nome
        self.preco_medio_por_kg = preco_medio_por_kg
    def __str__(self):
        return f'''
        Ingrediente: {self.nome}
        R$ / kg: {self.preco_medio_por_kg}'''

class Item:
    def __init__(self, receita, ingrediente, quantidade, medida, peso_aproximado):
        self.receita = receita
        self.ingrediente = ingrediente
        self.quantidade = quantidade
        self.medida = medida
        self.peso_aproximado = peso_aproximado
    def __str__(self):
        return f'''
        Receita: {self.receita}, 
        Ingrediente: {self.ingrediente},
        Quantidade: {self.quantidade},
        Medida: {self.medida},
        Peso aproximado: {self.medida}
        '''
    
bolacha_grao = Receita("Biscoito de grão-de-bico", 
                       "Em uma tigela misture todos os ingredientes misture bem. "+\
                       "Amasse a massa até ficar lisa. "+\
                       "Em uma assadeira, disponha 1 folha de papel manteiga e abra a massa. "+\
                        "Quanto mais fina, mais crocante ela ficará, apenas cuide para que não queime. "+\
                        "Corte no formato triangular ou retangular e asse em forno médio (180 °C) até dourar.",
                        "https://revistaampla.com.br/snacks-saudaveis-receita-de-biscoito-de-grao-de-bico/")

# https://www.google.com/search?q=pre%C3%A7o+farinha+gr%C3%A3o+de+bico&oq=pre%C3%A7o+farinha+gr%C3%A3o+de+bico&aqs=chrome..69i57j0i512l9.3520j0j7&sourceid=chrome&ie=UTF-8
farinha = Ingrediente("Farinha de grão de bico", "15")
paprica = Ingrediente("Páprica", "25")
azeite = Ingrediente("Azeite de Oliva Extravirgem", "50")
agua = Ingrediente("Água", "4")

i1 = Item(bolacha_grao, farinha, 2, "xícara de chá", "290 gr")
i2 = Item(bolacha_grao, paprica, 3, "colheres de sopa", "30 gr")
i3 = Item(bolacha_grao, azeite, 0.33, "xícara de chá", "80 ml")
i4 = Item(bolacha_grao, agua, 0.33, "xícara de chá", "80 ml")

print(bolacha_grao)

# como listar os itens de uma receita?
# :-(
for i in [i1, i2, i3, i4]:
    print(i)