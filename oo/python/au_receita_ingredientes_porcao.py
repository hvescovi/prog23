class Receita:
    def __init__(self, nome, modo_preparo, referencia):
        self.nome = nome
        self.modo_preparo = modo_preparo
        self.referencia = referencia
        self.porcoes = []
    def __str__(self):
        s = ""
        for p in self.porcoes:
            s += str(p) + "|"
        return f'''
        Receita: {self.nome},
        Modo de preparo: {self.modo_preparo},
        Referência: {self.referencia},
        Porções: {s}
        '''
    
class Ingrediente:
    def __init__(self, nome, preco_medio_por_kg):
        self.nome = nome
        self.preco_medio_por_kg = preco_medio_por_kg
    def __str__(self):
        return f'''
        Ingrediente: {self.nome}
        R$ / kg: {self.preco_medio_por_kg}'''

class Porcao:
    def __init__(self, ingrediente, quantidade, medida, peso_aproximado):
        self.ingrediente = ingrediente
        self.quantidade = quantidade
        self.medida = medida
        self.peso_aproximado = peso_aproximado
    def __str__(self):
        return f'''
        {self.ingrediente}, 
        Quantidade: {self.quantidade},
        Medida: {self.medida},
        Peso aproximado: {self.peso_aproximado}
        '''
    
bolacha_grao = Receita("Biscoito de grão-de-bico", 
                       "Em uma tigela misture todos os ingredientes misture bem. "+\
                       "Amasse a massa até ficar lisa. "+\
                       "Em uma assadeira, disponha 1 folha de papel manteiga e abra a massa. "+\
                        "Quanto mais fina, mais crocante ela ficará, apenas cuide para que não queime. "+\
                        "Corte no formato triangular ou retangular e asse em forno médio (180 °C) até dourar.",
                        "https://revistaampla.com.br/snacks-saudaveis-receita-de-biscoito-de-grao-de-bico/")

# https://www.google.com/search?q=pre%C3%A7o+farinha+gr%C3%A3o+de+bico&oq=pre%C3%A7o+farinha+gr%C3%A3o+de+bico&aqs=chrome..69i57j0i512l9.3520j0j7&sourceid=chrome&ie=UTF-8
farinha = Ingrediente("Farinha de grão de bico", "15")
paprica = Ingrediente("Páprica", "25")
azeite = Ingrediente("Azeite de Oliva Extravirgem", "50")
agua = Ingrediente("Água", "4")

p1 = Porcao(farinha, 2, "xícara de chá", "290 gr")
p2 = Porcao(paprica, 3, "colheres de sopa", "30 gr")
p3 = Porcao(azeite, 0.33, "xícara de chá", "80 ml")
p4 = Porcao(agua, 0.33, "xícara de chá", "80 ml")

# inserir porções na receita
bolacha_grao.porcoes = [p1, p2]
bolacha_grao.porcoes.append(p3)
bolacha_grao.porcoes.append(p4)

print(bolacha_grao)
