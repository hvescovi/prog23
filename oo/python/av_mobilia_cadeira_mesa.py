class Mobilia:
    def __init__(self, cor, material, altura, largura):
        self.cor = cor
        self.material = material
        self.altura = altura
        self.largura = largura
    def __str__(self):
        return f'''
        Cor: {self.cor}, Material: {self.material},
        Dimensões: {self.largura} x {self.altura}'''

class Cadeira(Mobilia):
    def __init__(self, cor, material, altura, largura, pernas, assento):
        super().__init__(cor, material, altura, largura)
        self.pernas = pernas # quantas pernas tem a cadeira?
        self.assento = assento # estofado, sintético, duro, etc
    def __str__(self):
        return f'''
        CADEIRA {super().__str__()}
        Pernas: {self.pernas}, Assento: {self.assento}'''

class Mesa(Mobilia):
    def __init__(self, cor, material, altura, largura, pernas, lugares):
        super().__init__(cor, material, altura, largura)
        self.pernas = pernas
        self.lugares = lugares # a mesa é para quantos lugares?
    def __str__(self):
        return f'''
        MESA {super().__str__()},
        Pernas: {self.pernas}, Lugares: {self.lugares}
        '''
    
class Estoque:
    def __init__(self, mobilia, quantidade):
        self.mobilia = mobilia
        self.quantidade = quantidade
    def __str__(self):
        return f'''
        {self.mobilia}, em estoque: {self.quantidade}'''
    
# teste das classes
cad1 = Cadeira("azul", "madeira", "30cm", "35cmx35cm", 4, "estofado")
cad2 = Cadeira("prata", "aço", "32cm", "33cmx30cm", 4, "metal")
m1 = Mesa ("madeira", "fibra de vidro", "1.5m", "2m x 1.5m", 4, 4)
estoque = [Estoque(cad1, 10), Estoque(cad2, 20), Estoque(m1, 8)]
for item_do_estoque in estoque:
    print(item_do_estoque)