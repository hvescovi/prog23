class Pessoa:
    def __init__(self, nome, email, telefone):
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self):
        return f'''
        Nome: {self.nome},
        Email: {self.email},
        Telefone: {self.telefone}''' 

class Paciente(Pessoa):
    def __init__(self, nome, email, dt_nasc, telefone, tps):
        super().__init__(nome, email, dt_nasc)
        self.telefone = telefone
        self.tipo_sanguineo = tps
    def __str__(self):
        return f'''
        {super().__str__()},
        Contato: {self.telefone}, 
        Tipo sanguíneo: {self.tipo_sanguineo}
        '''

class Medico(Pessoa):
    def __init__(self, nome, email, dt_nasc, crm, espec):
        super().__init__(nome, email, dt_nasc)
        self.CRM = crm
        self.especialidade = espec
    def __str__(self):
        return f'''
        {super().__str__()},
        CRM: {self.CRM}, 
        Especialidade: {self.especialidade}
        '''

class Consulta:
    def __init__(self, data, hora, paciente, medico):
        self.data = data
        self.hora = hora
        self.paciente = paciente
        self.medico = medico
    def __str__(self):
        return f'''Consulta no dia {self.data}, às {self.hora},
        Paciente: {str(self.paciente)}
        Médico: {str(self.medico)}'''

class ExameRealizado:
    def __init__(self, paciente, medico, nome_exame, resultado_exame, data):
        self.paciente = paciente
        self.medico = medico
        self.nome_exame = nome_exame
        self.resultado_exame = resultado_exame
        self.data = data
    def __str__(self):
        return f'''
        EXAME REALIZADO em {self.data}: {self.nome_exame}, resultado: {self.resultado_exame},
        paciente: {self.paciente}, médico que solicitou: {self.medico}
        '''

class Prescricao:
    def __init__(self, consulta, remedio, modo_uso):
        self.consulta = consulta
        self.remedio = remedio
        self.modo_uso = modo_uso
    def __str__(self):
        return f'''
        PRESCRIÇÃO: {self.remedio}, {self.modo_uso}, 
        prescrito na consulta: {self.consulta}'''
    
# testes...
pac = Paciente( "João da Silva", "josilva@gmail.com", "28/10/1988", "47 91234-5678", "O+")
med = Medico("Dr. John Fusacka", "jofusacka@gmail.com", "01/01/1990", "123ABC-9", "Homeopatia")
cons = Consulta("04/04/2023", "14:00", pac, med)
er1 = ExameRealizado(pac, med, "Vitamina B12", "240ng/ml", "05/04/2023")
er2 = ExameRealizado(pac, med, "Colesterol", "500ng/ml", "05/04/2023")
presc1 = Prescricao(cons, "Paracetamol 50mg", "1 vez antes de dormir, por 15 dias")

print(presc1)
print(er1, er2)