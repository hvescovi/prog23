class Carro:
    def __init__(self, placa, ano, modelo, cor):
        self.placa = placa
        self.ano = ano
        self.modelo = modelo
        self.cor = cor
    def __str__(self):
        return f'''
        Placa: {self.placa}, Ano: {self.ano},
        Modelo: {self.modelo}, Cor: {self.cor}'''

class Pessoa:
    def __init__(self, nome, email, telefone):
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self):
        return f'''
        Nome: {self.nome},
        Email: {self.email},
        Telefone: {self.telefone}'''
    
class Motorista(Pessoa):
    def __init__(self, nome, email, telefone, cnh):
        super().__init__(nome, email, telefone)
        self.cnh = cnh
    def __str__(self):
        return f'''
        {super().__str__()}, cnh: {self.cnh}'''

class Mecanico(Pessoa):
    def __init__(self, nome, email, telefone, matricula, salario):
        super().__init__(nome, email, telefone)
        self.cnh = matricula
        self.salario = salario
    def __str__(self):
        return f'''
        {super().__str__()}, 
        matrícula: {self.matricula}, salário: {self.salario}'''
    
class Conserto:
    def __init__(self, carro, mecanico, servico, peca, data, valor):
        self.carro = carro
        self.mecanico = mecanico
        self.servico = servico
        self.peca = peca
        self.data = data
        self.valor = valor
    def __str__(self):
        return f'''
        CONSERTO
        Serviço: {self.servico}, em {self.data}, R$ {self.valor},
        peça: {self.peca}
        Carro: {self.carro},
        Mecânico que executou o serviço: {self.mecanico}'''

fox = Carro("MDR1819", "2013", "Volkswagem Fox", "Prata")
# joao = Motorista("Joao da Silva", "josilva@gmail.com", "47 911223344", "192837-AB")
mec = Mecanico("Tiago Borges", "tibo@gmail.com", "47 902938475", "58859-3", "4000")
c1 = Conserto(fox, mec, "Regulagem da correia dentada", "", "04/04/2023", "100")
c2 = Conserto(fox, mec, "Troca de Óleo", "4 latas de óleo", "04/04/2023", "200")
registro = [c1, c2]
for r in registro:
    print(r)