class Computador:
    def __init__(self, tipo, marca, so):
        self.tipo = tipo # notebook ou desktop
        self.marca = marca
        self.sistema_operacional = so
        
c1 = Computador("Notebook", "Asus", "Windows")
print("Este computador é um ", c1.tipo, " da marca ", c1.marca, ", e possui instalado: " ,c1.sistema_operacional)

c2 = Computador("Desktop", "eu que montei", "Linux")
print("Este computador é um ", c2.tipo, " da marca ", c2.marca, ", e possui instalado: " ,c2.sistema_operacional)
