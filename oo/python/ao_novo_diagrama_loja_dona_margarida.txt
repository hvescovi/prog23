@startuml

class Cliente {
  nome
  endereco
  telefone
  email
}
class Produto {
  descricao
  preco
}
class Venda {
  produto: Produto
  quantidade
  data
}
class Equipamento {
  descricao
  dono: Cliente
}
class Servico {
  descricao
  preco
}
class OrdemDeServico #yellow {
  equipamento: Equipamento
  data_entrada
  data_notificacao
  data_entrega
  servicos: List of Servico
  orcamentos: List of OrcamentoAdicional
}
class Entrega {
  ordemDeServico: OrdemDeServico
  status
  preco
}
abstract class OrcamentoAdicional {
  ordemDeServico: OrdemDeServico
}

class OrcamentoDeServico {
  servico Servico
}
class OrcamentoDeProduto {
  produto Produto
}

Venda *-- Produto
Equipamento *-- Cliente
OrdemDeServico *-- Equipamento
Entrega *-- OrdemDeServico
OrcamentoAdicional *- OrdemDeServico
OrdemDeServico --*Servico
OrcamentoAdicional <|-- OrcamentoDeServico
OrcamentoAdicional <|-- OrcamentoDeProduto
OrcamentoDeServico --* Servico
OrcamentoDeProduto --* Produto
@enduml
