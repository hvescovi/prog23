# Exemplo para calcular a média de salarios de uma lista
# de objetos da classe Funcionario
# Obs.: exemplo feito em sala de aula dia 07/03/2023 - RODACKI

class Funcionario:
    def __init__(self, nome, salario):
        self.__nome = nome
        self.__salario = salario
    
    @property
    def nome(self):
        return self.__nome
    
    @property
    def salario(self):
        return self.__salario
    
    @salario.setter
    def salario(self, novo_salario):
        self.__salario = novo_salario     
# fim da definicao da classe

# *************************************************************
# Inicio do programa principal (que usa a classe Funcionario)
# *************************************************************

# criar lista vazia para armaxenar funcionarios
lista_funcionarios = []

# criar dois funcionários
f1 = Funcionario("João da Silva", 3000)
f2 = Funcionario("Maria Oliveira", 4000)
    
# inserir na lista
lista_funcionarios.append(f1)
lista_funcionarios.append(f2)
    
# calcular a média de salarios 
total_salarios = 0
# percorre a lista de funcionarios e acumula os valores de salarios
for i in range(len(lista_funcionarios)):
    total_salarios = total_salarios + lista_funcionarios[i].salario
 
media = total_salarios / len(lista_funcionarios)
print("Média", media)

# *****************************************************************
# Observacao: aqui temos outra maneira de construir o laço anterior
# *****************************************************************
total_salarios = 0
# percorre a lista de funcionarios e acumula os valores de salarios
for elemento in lista_funcionarios:
    total_salarios = total_salarios + elemento.salario
    
media = total_salarios / len(lista_funcionarios)
print(media)  
    
# Será que dá pra modificar o atributo nome?
try:
    f1.nome = "João da Silva Sauro"
    print("Novo nome:", f1.nome)
except:
    print("Não dá pra modificar o atributo nome, ele não tem @nome.setter")

# e o atributo __nome?
try:
    f1.__nome = "João da Silva Jones"
    print("Novo nome (atribuição via __nome): criamos um novo atributo :-( ", f1.__nome)
    print("Nome permanece (acesso via nome)", f1.nome)
    print("Novo atributo (acesso via __nome)", f1.__nome)
except:
    print("Não dá pra modificar o atributo __nome, ele é protegido")
