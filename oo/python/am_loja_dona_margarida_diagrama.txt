@startuml

class Cliente {
  nome
  endereco
  telefone
  email
}
class Produto {
  descricao
  preco
}
class Venda {
  produto: Produto
  quantidade
  data
}
class Equipamento {
  descricao
  dono: Cliente
}
class Servico {
  descricao
  valor
  equipamento: Equipamento
  data_entrada
  data_notificacao
  data_entrega
}
class Entrega {
  servico: Servico
  status
  preco
}
class Orcamento {
  servico Servico
  descricao
  valor
}
Venda *-- Produto
Venda *-- Cliente
Equipamento *-- Cliente
Servico *-- Equipamento
Entrega *- Servico
Orcamento *- Servico
@enduml
