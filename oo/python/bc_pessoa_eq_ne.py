class Pessoa:
    def __init__(self, nome, email, telefone):
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self): 
        return self.nome + ", " + self.email + ", " + self.telefone
    def __eq__(self, outro):
        return self.nome == outro.nome 

class Carro:
    def __init__(self, modelo, ano):
        self.modelo = modelo
        self.ano = ano

jo = Pessoa("João da Silva", "josilva@gmail.com", "47 912345678")
print(jo)
ma = Pessoa("Maria Oliveira", "maliv@gmail.com", "47 987654321")
print(ma)

print("jo é igual a ma? ")
print(jo == ma)

jo2 = Pessoa("João da Silva", "jojojo@gmail.com", "não tem")
print("jo é igual a jo2?")
print(jo == jo2)

c1 = Carro("Ford Ka", 2003)
c2 = Carro("Ford Ka", 2003)
print("c1 é igual a c2?")
print(c1 == c2)

# referenciando
c2 = c1
print("c1 é igual a c2?")
print(c1 == c2)