class Produto:
    def __init__(self, descricao, preco):
        self.descricao = descricao
        self.preco = preco

    def __str__(self):
        return f'''
        Descrição: {self.descricao},
        Preço: {self.preco}
        '''


descricao = "Mouse"
preco = "50,00"
novo_produto = Produto(descricao, preco)
print(novo_produto)

p2 = Produto("escova de dente", "37,00")
print(p2)


class Servico:
    def __init__(self, descricao, preco):
        self.descricao = descricao
        self.preco = preco

    def __str__(self):
        return f'''
        Descrição: {self.descricao},
        Preço: {self.preco}
        '''


descricao = "back-up"
preco = "120,00"
s1 = Servico(descricao, preco)
print(s1)

s2 = Servico("formatação com linux", "50")
print(s2)

class Cliente:
    def __init__(self, nome, email, endereco, telefone):
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.telefone = telefone

    def __str__(self):
        return f'''
        nome: {self.nome}
        email: {self.email}
        endereco: {self.endereco}
        telefone: {self.telefone}
        '''


novo_cliente = Cliente(
    "Anderson", "andersongamespronomine@gmail.com", "rua 15", "47 988775544")
print(novo_cliente)

class Equipamento:
    def __init__(self,descricao,dono):
        self.descricao=descricao
        self.dono=dono
    def __str__(self):
        return f'''
        Descrição:{self.descricao}
        Dono:{self.dono}
        '''
computadori3 = Equipamento ("intel i3, 8 gb, 1thd", novo_cliente)
print (computadori3)

class Venda:
    def __init__(self, produto, quantidade, data):
        self.produto= produto
        self.quantidade= quantidade
        self.data = data
    def __str__(self):
        return f'''
        produto:{self.produto}
        quantidade:{self.quantidade}
        data:{self.data}'''

venda1 = Venda(p2, 3, "07/06/2020")
print(venda1)

class OrdemDeServico:
    def __init__(self, equipamento, data_entrada, data_notificacao, data_entrega_ou_retirada, servicos, produtos):
        self.equipamento = equipamento
        self.data_entrada = data_entrada
        self.data_notificacao = data_notificacao
        self.data_entrega_ou_retirada = data_entrega_ou_retirada
        self.servicos = servicos
        self.produtos = produtos
    def __str__(self):
        servs = ""
        for s in self.servicos:
            servs = servs + f'\nServiço: {s}'
        
        return f'''
        ORDEM DE SERVIÇO 
        ****************
        Equipamento: {self.equipamento}
        Data de Entrada: {self.data_entrada}
        Data da Notificação: {self.data_notificacao}
        Data de Entrega ou Retirada: {self.data_entrega_ou_retirada}
        Serviços: {servs}
        Produtos: {self.produtos}'
        '''    

ordem_de_servico = OrdemDeServico(computadori3, "16/03/2023", "", "", [s1, s2], [p2])
print(ordem_de_servico)