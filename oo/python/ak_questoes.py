class Questao:
    def __init__(self, enunciado, autor, data_cadastro):
        self.enunciado = enunciado
        self.autor = autor
        self.data_cadastro = data_cadastro
    def __str__(self):
        return f'{self.enunciado}, {self.autor}, '+\
               f'{self.data_cadastro}'        

class QuestaoAberta(Questao):
    def __init__(self, enunciado, autor, data_cadastro, gabarito):
        super().__init__(enunciado, autor, data_cadastro)
        self.gabarito = gabarito
    def __str__(self):
        #return f'{self.enunciado}, {self.autor}, '+\
        #       f'{self.data_cadastro},{self.gabarito}'
        return f'{super().__str__()}, {self.gabarito}'

q_aberta = QuestaoAberta(
    "Qual é o nome do método que expressa o objeto em formato texto?",
    "Hylson",
    "08/03/2023",
    "__str__")
print(q_aberta)

class Respondente:
    def __init__(self, nome, email, observacao):
        self.nome = nome
        self.email = email
        self.observacao = observacao
    def __str__(self):
        return f'{self.nome}, {self.email}, {self.observacao}'
    
r1 = Respondente("Teresa", "te@gmail.com", "sem obs")
print(r1)

class Resposta:
    def __init__(self, respondente, questao, resposta_textual, data_hora):
        self.respondente = respondente
        self.questao = questao
        self.resposta_textual = resposta_textual
        self.data_hora = data_hora
    def __str__(self):
        return f'\nRespondente: {self.respondente}, \nQuestão: {self.questao}, '+\
        f'\nDados da resposta: {self.resposta_textual}, {self.data_hora}'

resp1 = Resposta(r1, q_aberta, "__init__", "08/03/2023")
print(resp1)
