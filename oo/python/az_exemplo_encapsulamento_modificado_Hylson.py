# ENCAPSULAMENTO é o recusto que temos para ocultar (ou encapsular, "colocar
# em uma cápsula") atributos e/ou métodos de uma classe
# quando temos um atributo ou método encapsulado, dizemos que é um
# atributo PRIVADO ou método PRIVADO. Isto significa que não pode ser
# acessado diretamente fora da classe, ficando assim protegido de 
# eventuais alterações indesejadas ou incorretas.
#
# A classe Pessoa tem um atributo publico e um atributo privado:
# Atrib. publico: self.nome
# Atrib. privado: self.__idade

# RODACKI

class Pessoa:
    # o metodo construtor está verificando se parametro idade é
    # positivo ou negativo. Se for negativo, ele inverte o sinal antes 
    # de atribuir o valor ao atributo self.__idade
    # desta forma, garante que não sejam criados objetos Pessoa
    # com idade negativa, o que seria incorreto
    # 
    def __init__(self, nome, idade):
        self.nome = nome
        if idade < 0:
            self.__idade = -idade
        else:
            self.__idade = idade
            
    # como o atributo __idade é PRIVADO, ele não pode ser 
    # acessado diretamente de fora da classe. Então, criamos
    # o metodo acessor setIdade para alterar o valor da idade
    # do objeto Pessoa. Como isso é feito via um método, podemos
    # incluir a lógica para evitar que sejam atribuídas idades
    # negativas
    def setIdade(self, novaIdade):
        if novaIdade < 0:
            self.__idade = -novaIdade
        else:
            self.__idade = novaIdade
            
    # se quisermos ler (receber) fora da classe um atributo privado,
    # precisamos criar um método acessor tipo "getter", que retorna o 
    # valor do atrubuto para quem chamou lá fora da classe    
    def getIdade(self):
        return self.__idade
    
    
    # No Python, os DECORADORES @property e @atributo.setter permitem que
    # criemos acesso de alteracao e de leitura de atributos privados 
    # (tanto metodos acessores set quanto get), mas com uma sintaxe 
    # semelhante a acessar o atributo diretamente, por exemplo:
    # obj.idade = 15  ou print(objeto.idade)
    
    # em muitas linguagens de programação, este recurso é chamado
    # de COMPUTED PROPERTIES
    
    # esta property faz o papel de metodo getIdade
    @property
    def idade(self):
        return self.__idade
    
    # esta property faz o papel de setIdade
    @idade.setter
    def idadeee(self, novaIdade):
        if novaIdade < 0:
            self.__idade = -novaIdade
        else:
            self.__idade = novaIdade
        
    def apresentar(self):
        print(self.nome, self.__idade)
        
# fim da definicao da classe        

# inicio do programa
# cria um objeto da classe pessoa (note que o metodo construtor não
# vai permitir que a idade fique negativa lá dentro do objeto)
p1 = Pessoa("Luiz", -17)

p1.apresentar()

# obtém o valor da idade via metodo
print(p1.getIdade())
# obtém o valor da idade via "property", note que aqui
# fora da classe, é como se eu estivessa acessando o 
# atributo diretamente: obj.atributo
print(p1.idadeee)

# altera a idade usando método setter
p1.setIdade(19)
print(p1.idadeee)

# altera a idade usando property setter
p1.idadeee = -20
print(p1.idadeee)

# dá pra acessar o atributo oculto?
try:
    print("Atributo oculto: ", p1.__idade)
except:
    print("Não deu certo para acessar o p1.__idade")
