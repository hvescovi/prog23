package com.mycompany.database.maven;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DAO {

    // construtor
    public Connection retornarConexao() {

        // abrir conexão
        Connection conn = null;
        try {
            // COPIAR O BANCO DE DADOS para uma pasta temporária
            // exemplo: /tmp/
            String caminho = "/tmp/";
            // para WINDOWS, deve ser algo assim: c:\temp\";
            
            // db parameters
            String url = "jdbc:sqlite:"+caminho+"pessoas.db";
            
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            return conn;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        // retorno nulo, caso aconteça erro :-(
        return null;
    }

    public ArrayList<Pessoa> retornar_pessoas() {
        try {
            Connection conn = retornarConexao();
            PreparedStatement stmt = conn.prepareStatement("select * from pessoa;");
            ResultSet resultSet = stmt.executeQuery();

            ArrayList<Pessoa> retorno = new ArrayList();
            
            while (resultSet.next()) {
                Pessoa p = new Pessoa();
                p.setId(resultSet.getInt("id"));
                p.setNome(resultSet.getString("nome"));
                p.setEmail(resultSet.getString("email"));
                p.setTelefone(resultSet.getString("telefone"));
                
                retorno.add(p);
            }
            conn.close();
            return retorno;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        // retorno vazio, caso aconteça erro :-(
        return new ArrayList();
    }
    
    public void incluirPessoa(Pessoa nova){
        
        try {
            Connection conn = retornarConexao();
            
            String sql = "INSERT INTO Pessoa VALUES (null, ?, ?, ?)";
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1, nova.getNome());
            stmt.setString(2, nova.getEmail());
            stmt.setString(3, nova.getTelefone());
            
             // inserindo 
            stmt.execute();

            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }        
    }

}
