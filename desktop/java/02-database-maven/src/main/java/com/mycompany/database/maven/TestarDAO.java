/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.maven;

/**
 *
 * @author friend
 */
public class TestarDAO {
    public static void main(String args[]){
        
        // listagem
        DAO dao = new DAO();
        for (Pessoa p: dao.retornar_pessoas()) {
            System.out.println(p);                    
        }
        
        Pessoa alguem = new Pessoa();
        alguem.setNome("João da Silva");
        alguem.setEmail("josilva@gmail.com");
        alguem.setTelefone("47 9 1234 5678");
        
        // incluindo
        dao.incluirPessoa(alguem);
        System.out.println("Pessoa incluída");
    }
    
}