/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.maven;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestarDriver {
    public void testar() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:pessoas.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            
            System.out.println("Connection to SQLite has been established.");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    
    public static void main(String args[]) {
        TestarDriver ta = new TestarDriver();
        ta.testar();
    }
        
}
