/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package pkg01.basic;

/**
 *
 * @author friend
 */
public class Basic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Janela01 j1 = new Janela01();
        j1.setLocationRelativeTo(null); // centralizar
        j1.setVisible(true);
    }
    
    /*
    observações:
    - centralizar a janela
    - modificar a propriedade "Variable Name" da guia "Code", na propriedade 
    do JTextField, para acessar o valor da caixa de entrada
    - formatar o label com HTML para quebrar linhas :-)
    */
    
}
